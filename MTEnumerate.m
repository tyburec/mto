function [BestDesignMat, BestObjective, Misc] = MTEnumerate(Func, DesignVarMat, EOpt)
%MTENUMERATION Brute-force enumeration
%
%   INPUT:
%           Fcn                 evaluated objective function handle(s)
%           DesignVarMat        custom matrix of design variables
%           EOpt                EOpt structure
%
%   OUTPUT:
%           BestDesignMat       matrix of best design variables
%           BestObjective       best objective function value
%           Misc.ObjVals        vector of all objectives
%
% (c) Copyright 2016 Marek Tyburec, marek.tyburec@fsv.cvut.cz
%
%
% This file is part of Modular-Topology Optimization of Truss Structures Composed of Wang Tiles.
% 
% Modular-Topology Optimization of Truss Structures Composed of Wang Tiles
% is free software: you can redistribute it and/or modify it under the terms of the 
% GNU General Public License as published by the Free Software Foundation, 
% either version 3 of the License, or any later version.
% 
% Modular-Topology Optimization of Truss Structures Composed of Wang Tiles
% is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
% without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Modular-Topology Optimization of Truss Structures Composed of Wang Tiles.
% If not, see <http://www.gnu.org/licenses/>.

if ~exist('EOpt', 'var')
    EOpt = struct();
end

if ~exist('Func', 'var')
    error('MTEnumerate: Func needs to be specified.');
end
if (numel(Func) == 1) && ~isa(Func, 'function_handle')
    error('MTEnumerate: Func needs to be a function handle.');
elseif (numel(Func) == 1)
    EOpt.UseParallel = false;
elseif (numel(Func)>1) && (~iscell(Func) || ...
        (sum(arrayfun(@(x) isa(Func{x}, 'function_handle'), 1:length(Func)))~=length(Func)))
    error('MTEnumerate: Func can be entered as a single function handle or as a cell object of function handles.');
else
    EOpt.UseParallel = true;
    EOpt.NumWorkers = length(Func);
end

if ~exist('DesignVarMat', 'var')
    error('MTEnumerate: DesignVarMat needs to be specified.');
end

if ~isfield(EOpt, 'RangeMin')
    EOpt.RangeMin = 0;
elseif ~isnumeric(EOpt.RangeMin)
    error('MTEnumerate: EOpt.RangeMin needs to be assigned a numeric value.');
elseif length(EOpt.RangeMin(:))~=1
    error('MTEnumerate: EOpt.RangeMin needs to be assigned a single number.');
elseif (EOpt.RangeMin < 0)
    error('MTEnumerate: EOpt.RangeMin needs to be assigned a non-negative value.');
end

if ~isfield(EOpt, 'RangeMax')
    EOpt.RangeMax = 2^(size(DesignVarMat,1) * size(DesignVarMat,2))-1;
elseif ~isnumeric(EOpt.RangeMax)
    error('MTEnumerate: EOpt.RangeMax needs to be assigned a numeric value.');
elseif length(EOpt.RangeMax(:))~=1
    error('MTEnumerate: EOpt.RangeMax needs to be assigned a single number.');
elseif (EOpt.RangeMax < 0)
    error('MTEnumerate: EOpt.RangeMax needs to be assigned a non-negative value.');
elseif (EOpt.RangeMin > EOpt.RangeMax)
    error('MTEnumerate: EOpt.RangeMax needs to be assigned a value greater or equal to EOpt.RangeMin.');
end

if ~isfield(EOpt, 'UseParallel')
    EOpt.UseParallel = false;
elseif ~islogical(EOpt.UseParallel)
    error('MTEnumeration: EOpt.UseParallel needs to be assigned a logical value.');
elseif length(EOpt.UseParallel(:))~=1
    error('MTEnumeration: EOpt.UseParallel needs to be assigned a single logical value.');
end

if ~isfield(EOpt, 'Display')
    EOpt.Display = 'on';
elseif ~ischar(EOpt.Display)
    error('MTEnumeration: EOpt.Display needs to be assigned an array of chars.');
elseif ~strcmp(EOpt.Display, 'on') || ~strcmp(EOpt.Display, 'off')
    error('MTEnumeration: EOpt.Display can be set either on or off.');
end

if ~isfield(EOpt, 'DisplayFreq')
    EOpt.DisplayFreq = 10;
elseif ~isnumeric(EOpt.DisplayFreq)
    error('MTEnumerate: EOpt.DisplayFreq needs to be assigned a numeric value.');
elseif length(EOpt.DisplayFreq(:))~=1
    error('MTEnumerate: EOpt.DisplayFreq needs to be assigned a single number.');
elseif (EOpt.DisplayFreq < 0)
    error('MTEnumerate: EOpt.DisplayFreq needs to be assigned a non-negative value.');
end

if strcmp(EOpt.Display, 'on')
    fprintf('MTEnumeration: starting enumeration...\n');
end

%% Main part
Values = EOpt.RangeMin:EOpt.RangeMax;
ObjVals = zeros(numel(Values),1);

if EOpt.UseParallel
    if ~isempty(gcp('nocreate'))
        PoolObj = gcp('nocreate');
        if PoolObj.NumWorkers ~= EOpt.NumWorkers
            delete(PoolObj);
            Cluster = parcluster('local');
            Cluster.NumWorkers = EOpt.NumWorkers;
            PoolObj = parpool(Cluster, EOpt.NumWorkers);
        end
    else
        Cluster = parcluster('local');
        Cluster.NumWorkers = EOpt.NumWorkers;
        PoolObj = parpool(Cluster, EOpt.NumWorkers);
    end
    spmd
        for j = labindex:EOpt.NumWorkers:numel(Values)
            DesignVals = reshape(de2bi(Values(j), numel(DesignVarMat)), size(DesignVarMat,1), size(DesignVarMat,2));
            ObjVals(j) = feval(Func{labindex}, DesignVals);
            if strcmp(EOpt.Display, 'on') && (mod((j-labindex)/EOpt.NumWorkers,EOpt.DisplayFreq) == EOpt.DisplayFreq-1)
                fprintf('MTEnumeration: Lab %d: Iter %d, best objective %f.\n',labindex, j, min(ObjVals(ObjVals~=0)));
            end
        end
    end
    ObjVals = sum([ObjVals{:}],2);
else
    for j=1:numel(Values)
        DesignVals = reshape(de2bi(Values(j), numel(DesignVarMat)), size(DesignVarMat,1), size(DesignVarMat,2));
        ObjVals(j) = feval(Func, DesignVals);
        if strcmp(EOpt.Display, 'on') && (mod(j,EOptFreq)==0)
        	fprintf('MTEnumeration: Iter %d, best objective %f.\n', j, min(ObjVals(ObjVals~=0)));
       	end
    end
end

if EOpt.UseParallel
    delete(PoolObj);
end

Misc.ObjVals = ObjVals;
BestDesignMat = reshape(de2bi(Values(ObjVals==min(ObjVals)), ...
    numel(DesignVarMat)), size(DesignVarMat,1), size(DesignVarMat,2));
BestObjective = min(ObjVals);

end
