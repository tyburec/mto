# Modular-topology optimization with Wang tilings: An application to truss structures

This project considers the problem of finding the optimal set of modules, formalized by Wang tiles, 
for user-defined design domains. Wang tiles allow to control the numbers of modules and the interface 
types and secure continuity of the material distribution across neighboring tiles. In the current state, 
this project supports only 2d corner Wang tilings over 2 colors.

The proposed method works as follows:
- user defines a rectangular design domain, holes, boundary conditions, and discretization;
- concurrent optimization of the assembly plan and modules topologies follows:
    - assembly plan admits optimization using Simulated Annealing with adaptive neighborhood, 
      or Genetic Algorithm. Both these methods exhibit a similar performance;
    - module topologies are optimized using Second-Order Cone Programming or SemiDefinite Programming.
      The former approach handles stress constraints and is notably faster;
    - optimized designs can be exported.

## Usage
The scripts have been written in MATLAB; it is highly recommended to use at least **MATLAB R2016b**, 
since the code runs there significantly faster (issues with repeated calls to methods).
The optimization is modeled through [**YALMIP**](https://yalmip.github.io/), it is thus neccessary 
to have it [installed](https://yalmip.github.io/download/). Moreover, YALMIP needs to have an access to solvers appropriate for
the selected formulation. For more information, refer to the [YALMIP solver section](https://yalmip.github.io/allsolvers/).
Example beam structure is defined in `MTSimpleBeam.m`.

## License
See the COPYING file.

## Contact
marek.tyburec@fsv.cvut.cz