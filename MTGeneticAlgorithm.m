function [BestChromosome, BestFitnessVal, Misc] = MTGeneticAlgorithm(FitnessFcn, Chromosome, GAOpt)
%MTGENETICALGORITHM Custom genetic algorithm implementation
%
%  Based on:
%   Zelinka, Ivan. Evoluční Výpočetní Techniky: Principy a Aplikace. 
%     Praha: BEN - Technická Literatura, 2009.
%
%   INPUT:
%           FitnessFcn          evaluated fitness function handle
%           Chromosome.X        1st size of binary chromosome matrix
%           Chromosome.Y        2nd size of binary chromosome matrix
%           GAOpt               GAOpt structure
%
%   OUTPUT:
%           BestChromosome      best chromosome
%           BestFitnessVal      best fitness value
%           Misc.FitnessPath    history of fitnesses
%
% (c) Copyright 2016 Marek Tyburec, marek.tyburec@fsv.cvut.cz
%
%
% This file is part of Modular-Topology Optimization of Truss Structures Composed of Wang Tiles.
% 
% Modular-Topology Optimization of Truss Structures Composed of Wang Tiles
% is free software: you can redistribute it and/or modify it under the terms of the 
% GNU General Public License as published by the Free Software Foundation, 
% either version 3 of the License, or any later version.
% 
% Modular-Topology Optimization of Truss Structures Composed of Wang Tiles
% is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
% without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Modular-Topology Optimization of Truss Structures Composed of Wang Tiles.
% If not, see <http://www.gnu.org/licenses/>.

% TODO:
% Inverse tournament
% Similarity to global optimum

%% Settings checking
% OPTIONS
if ~exist('GAOpt', 'var')
    GAOpt = struct();
end

% INPUTS
if ~exist('FitnessFcn', 'var')
    error('MTGeneticAlgorithm: FitnessFcn needs to be specified.');
end
if (numel(FitnessFcn) == 1) && ~isa(FitnessFcn, 'function_handle')
    error('MTGeneticAlgorithm: FitnessFcn needs to be a function handle.');
elseif iscell(FitnessFcn) && ...
        (sum(arrayfun(@(x) isa(FitnessFcn{x}, 'function_handle'), 1:length(FitnessFcn)))~=length(FitnessFcn))
    error('MTGeneticAlgorithm: FitnessFcn can be entered as a single function handle or as a cell object of function handles.');
elseif iscell(FitnessFcn)
    GAOpt.UseParallel = true;
    GAOpt.NumWorkers = length(FitnessFcn);
else
    GAOpt.UseParallel = false;
    GAOpt.NumWorkers = 1;
end
    
if ~exist('Chromosome', 'var')
    error('MTGeneticAlgorithm: Chromosome needs to be specified.');
end
if ~isfield(Chromosome, 'X')
    error('MTGeneticAlgorithm: Chromosome.X needs to be specified.');
elseif length(Chromosome.X(:))~=1
    error('MTGeneticAlgorithm: Chromosome.X needs to be assigned a single number.');
elseif (Chromosome.X <= 0)
    error('MTGeneticAlgorithm: Chromosome.X needs to be assigned a positive value.');
end  
if ~isfield(Chromosome, 'Y')
    error('MTGeneticAlgorithm: Chromosome.Y needs to be specified.');
elseif length(Chromosome.Y(:))~=1
    error('MTGeneticAlgorithm: Chromosome.Y needs to be assigned a single number.');
elseif (Chromosome.Y <= 0)
    error('MTGeneticAlgorithm: Chromosome.Y needs to be assigned a positive value.');
end    

if ~isfield(GAOpt, 'FitnessScaleMin')
    GAOpt.FitnessScaleMin = 1e-3;
elseif ~isnumeric(GAOpt.FitnessScaleMin)
    error('MTGeneticAlgorithm: GAOpt.FitnessScaleMin needs to be assigned a numeric value.');
elseif length(GAOpt.FitnessScaleMin(:))~=1
    error('MTGeneticAlgorithm: GAOpt.FitnessScaleMin needs to be assigned a single number.');
elseif (GAOpt.FitnessScaleMin <= 0) || (GAOpt.FitnessScaleMin >= 1)
    error('MTGeneticAlgorithm: GAOpt.FitnessScaleMin needs to be assigned a value in range (0; 1).');
end

if ~isfield(GAOpt, 'PopulationSize')
    GAOpt.PopulationSize = round(sqrt(Chromosome.X*Chromosome.Y)*4);
elseif ~isnumeric(GAOpt.PopulationSize)
    error('MTGeneticAlgorithm: GAOpt.PopulationSize needs to be assigned a numeric value.');
elseif length(GAOpt.PopulationSize(:))~=1
    error('MTGeneticAlgorithm: GAOpt.PopulationSize needs to be assigned a single number.');
elseif (GAOpt.PopulationSize <= 0)
    error('MTGeneticAlgorithm: GAOpt.PopulationSize needs to be assigned a positive value.');
end

if ~isfield(GAOpt, 'GenerationsCount')
    GAOpt.GenerationsCount = round(sqrt(Chromosome.X*Chromosome.Y)*8);
elseif ~isnumeric(GAOpt.GenerationsCount)
    error('MTGeneticAlgorithm: GAOpt.GenerationsCount needs to be assigned a numeric value.');
elseif length(GAOpt.GenerationsCount(:))~=1
    error('MTGeneticAlgorithm: GAOpt.GenerationsCount needs to be assigned a single number.');
elseif (GAOpt.GenerationsCount <= 0)
    error('MTGeneticAlgorithm: GAOpt.GenerationsCount needs to be assigned a positive value.');
end

if ~isfield(GAOpt, 'SelectionType')
    GAOpt.SelectionType = 'tournament-selection';
elseif ~ischar(GAOpt.SelectionType)
    error('MTGeneticAlgorithm: GAOpt.SelectionType needs to be assigned an array of chars.');
elseif ~strcmp(GAOpt.SelectionType, 'tournament-selection') && ...
       ~strcmp(GAOpt.SelectionType, 'roulette-wheel') && ...
       ~strcmp(GAOpt.SelectionType, 'rank-selection')
    error('MTGeneticAlgorithm: Unknown GAOpt.SelectionType.');
end

if ~isfield(GAOpt, 'CrossoverType')
    GAOpt.CrossoverType = 'uniform';
elseif ~ischar(GAOpt.CrossoverType)
    error('MTGeneticAlgorithm: GAOpt.CrossoverType needs to be assigned an array of chars.');
elseif ~strcmp(GAOpt.CrossoverType, 'uniform')
    error('MTGeneticAlgorithm: Unknown GAOpt.CrossoverType.');
end

if ~isfield(GAOpt, 'Tournament')
    GAOpt.Tournament = struct();
end

if ~isfield(GAOpt.Tournament, 'Size')
    GAOpt.Tournament.Size = round(GAOpt.PopulationSize/3);
elseif ~isnumeric(GAOpt.Tournament.Size)
    error('MTGeneticAlgorithm: GAOpt.Tournament.Size needs to be assigned a numeric value.');
elseif length(GAOpt.Tournament.Size(:))~=1
    error('MTGeneticAlgorithm: GAOpt.Tournament.Size needs to be assigned a single number.');
elseif (GAOpt.Tournament.Size <= 0)
    error('MTGeneticAlgorithm: GAOpt.Tournament.Size needs to be assigned a positive value.');
end

if ~isfield(GAOpt.Tournament, 'Probability')
    GAOpt.Tournament.Probability = .3;
elseif ~isnumeric(GAOpt.Tournament.Probability)
    error('MTGeneticAlgorithm: GAOpt.Tournament.Probability needs to be assigned a numeric value.');
elseif length(GAOpt.Tournament.Probability(:))~=1
    error('MTGeneticAlgorithm: GAOpt.Tournament.Probability needs to be assigned a single number.');
elseif (GAOpt.Tournament.Probability < 0) || (GAOpt.Tournament.Probability > 1)
    error('MTGeneticAlgorithm: GAOpt.Tournament.Probability needs to be assigned a value in range (0; 1).');
end

if ~isfield(GAOpt, 'CrossoverProbability')
    GAOpt.CrossoverProbability = .95;
elseif ~isnumeric(GAOpt.CrossoverProbability)
    error('MTGeneticAlgorithm: GAOpt.CrossoverProbability needs to be assigned a numeric value.');
elseif length(GAOpt.CrossoverProbability(:))~=1
    error('MTGeneticAlgorithm: GAOpt.CrossoverProbability needs to be assigned a single number.');
elseif (GAOpt.CrossoverProbability < 0) || (GAOpt.CrossoverProbability > 1)
    error('MTGeneticAlgorithm: GAOpt.CrossoverProbability needs to be assigned a value in range (0; 1).');
end

if ~isfield(GAOpt, 'MutationProbability')
    GAOpt.MutationProbability = 1/(Chromosome.X*Chromosome.Y);
elseif ~isnumeric(GAOpt.MutationProbability)
    error('MTGeneticAlgorithm: GAOpt.MutationProbability needs to be assigned a numeric value.');
elseif length(GAOpt.MutationProbability(:))~=1
    error('MTGeneticAlgorithm: GAOpt.MutationProbability needs to be assigned a single number.');
elseif (GAOpt.MutationProbability < 0) || (GAOpt.MutationProbability > 1)
    error('MTGeneticAlgorithm: GAOpt.MutationProbability needs to be assigned a value in range (0; 1).');
end

if ~isfield(GAOpt, 'Elitism')
    GAOpt.Elitism = true;
elseif ~islogical(GAOpt.Elitism)
    error('MTGeneticAlgorithm: GAOpt.Elitism needs to be assigned a logical value.');
elseif length(GAOpt.Elitism(:))~=1
    error('MTGeneticAlgorithm: GAOpt.Elitism needs to be assigned a single logical value.');
end

if ~isfield(GAOpt, 'Diversity')
    GAOpt.Diversity = false;
elseif ~islogical(GAOpt.Diversity)
    error('MTGeneticAlgorithm: GAOpt.Diversity needs to be assigned a logical value.');
elseif length(GAOpt.Diversity(:))~=1
    error('MTGeneticAlgorithm: GAOpt.Diversity needs to be assigned a single logical value.');
end

if ~isfield(GAOpt, 'UseParallel')
    GAOpt.UseParallel = false;
elseif ~islogical(GAOpt.UseParallel)
    error('MTGeneticAlgorithm: GAOpt.UseParallel needs to be assigned a logical value.');
elseif length(GAOpt.UseParallel(:))~=1
    error('MTGeneticAlgorithm: GAOpt.UseParallel needs to be assigned a single logical value.');
end

if ~isfield(GAOpt, 'Display')
    GAOpt.Display = 'on';
elseif ~ischar(GAOpt.Display)
    error('MTGeneticAlgorithm: GAOpt.Display needs to be assigned an array of chars.');
elseif ~strcmp(GAOpt.Display, 'on') || ~strcmp(GAOpt.Display, 'off')
    error('MTGeneticAlgorithm: GAOpt.Display can be set either on or off.');
end

if ~isfield(GAOpt, 'FixVals')
    GAOpt.FixVals = [];
end
    
if strcmp(GAOpt.Display, 'on')
    fprintf('MTGeneticAlgorithm: starting optimization...');
end

%% Generate random population of chromosomes
Population = arrayfun(@(x) randi([0 1], Chromosome.Y, Chromosome.X), ...
    1:GAOpt.PopulationSize, 'UniformOutput', false);

% fix requested values
if isempty(GAOpt.FixVals)
    FixedIndex = [];
else
    FixedIndex = sub2ind([Chromosome.Y Chromosome.X], GAOpt.FixVals(:,1), GAOpt.FixVals(:,2));
    for i=1:GAOpt.PopulationSize
        Population{i}(FixedIndex) = GAOpt.FixVals(:,3);
    end
end

%% Evaluate the fitness of each chromosome in population
FitnessVal = zeros(GAOpt.PopulationSize,1);
if GAOpt.UseParallel
    if ~isempty(gcp('nocreate'))
        PoolObj = gcp('nocreate');
        if PoolObj.NumWorkers ~= GAOpt.NumWorkers
            delete(PoolObj);
            Cluster = parcluster('local');
            Cluster.NumWorkers = GAOpt.NumWorkers;
            PoolObj = parpool(Cluster, GAOpt.NumWorkers);
        end
    else
        Cluster = parcluster('local');
        Cluster.NumWorkers = GAOpt.NumWorkers;
        PoolObj = parpool(Cluster, GAOpt.NumWorkers);
    end
    
    spmd
        for j = labindex:GAOpt.NumWorkers:GAOpt.PopulationSize
            FitnessVal(j) = feval(FitnessFcn{labindex}, Population{j});
        end
    end
    FitnessVal = sum([FitnessVal{:}],2);
else
    for j=1:GAOpt.PopulationSize
        FitnessVal(j) = feval(FitnessFcn, Population{j});
    end
end

% perform scaling
ScaledFitnessVal = fitness_scale(FitnessVal, GAOpt.FitnessScaleMin);
Misc.FitnessPath{1} = FitnessVal;
Misc.Population{1} = Population;

% printing
if strcmp(GAOpt.Display, 'on')
    fprintf('MTGeneticAlgorithm: Generation %d/%d:\tFitness = %f,\tmeanFit = %f.\n',...
        0, GAOpt.GenerationsCount, min(FitnessVal), mean(FitnessVal));
end


%% Loop through lives [generations]
for i=1:GAOpt.GenerationsCount
        
    %% Selection
    %  of parents though roulette wheel
    ParentsA = zeros(GAOpt.PopulationSize,1);
    ParentsB = zeros(GAOpt.PopulationSize,1);
    switch GAOpt.SelectionType
        case 'roulette-wheel'
            for j=1:GAOpt.PopulationSize
                R = rand(1) * sum(ScaledFitnessVal);
                ParentsA(j) = nnz(R >= cumsum([0; ScaledFitnessVal(1:end-1)]));
                while 1
                    R = rand(1) * sum(ScaledFitnessVal);
                    ParentsB(j) = nnz(R >= cumsum([0; ScaledFitnessVal(1:end-1)]));
                    if ParentsB(j)~=ParentsA(j)
                        break;
                    end
                end
            end
        case 'rank-selection'
            [~, SortedFitness] = sort(ScaledFitnessVal, 'ascend');
            CumSums = arrayfun(@(x) sum(1:x), SortedFitness);
            for j=1:GAOpt.PopulationSize
                R = rand(1) * sum(1:GAOpt.PopulationSize);
                ParentsA(j) = nnz(R >= [0; CumSums(1:end-1)]);
                while 1
                    R = rand(1) * sum(1:GAOpt.PopulationSize);
                    ParentsB(j) = nnz(R >= [0; CumSums(1:end-1)]);
                    if ParentsB(j)~=ParentsA(j)
                        break;
                    end
                end
            end
        case 'tournament-selection'
            for j=1:GAOpt.PopulationSize
                RandomKnightsA = randperm(GAOpt.PopulationSize, ...
                    GAOpt.Tournament.Size);                                 % randomly choose chromosomes for first parent
                RandomKnightsB = randperm(GAOpt.PopulationSize, ...
                    GAOpt.Tournament.Size);                                 % randomly choose chromosomes for second parent
                [~, KnightsOrderA] = sort(ScaledFitnessVal(RandomKnightsA),...
                    'descend');                                             % sort chromosomes according to fitness
                [~, KnightsOrderB] = sort(ScaledFitnessVal(RandomKnightsB),...
                    'descend');                                             % sort chromosomes according to fitness
                KnightsOrderA = RandomKnightsA(KnightsOrderA);              % create first tournament chromosome ordered set
                KnightsOrderB = RandomKnightsB(KnightsOrderB);              % create second tournament chromosome ordered set
                Probabilities = arrayfun(@(x) GAOpt.Tournament.Probability ...
                    * (1-GAOpt.Tournament.Probability)^x,...
                    0:GAOpt.Tournament.Size-1);                             % vector of probebilities for each chromosome in tournament
                R = rand(1) * sum(Probabilities);                           % random value for first parent
                ParentsA(j) = KnightsOrderA(nnz(R >= ...
                    [0 cumsum(Probabilities(1:end-1))]));                   % find number of first parent                
                if Probabilities(1)<1
                    iter = 0;
                    while 1
                        iter = iter+1;
                        R = rand(1) * sum(Probabilities);
                        ParentsB(j) = KnightsOrderB(nnz(R >= ...
                            [0 cumsum(Probabilities(1:end-1))]));
                        if ParentsB(j)~=ParentsA(j)
                            break;
                        end
                        if iter == 50
                            error('MTGeneticAlgorithm: This should not happen!');
                        end
                    end
                else
                    R = rand(1) * sum(Probabilities);
                    ParentsB(j) = KnightsOrderB(nnz(R >= ...
                        [0 cumsum(Probabilities(1:end-1))]));
                end
            end
        otherwise
            error('MTGeneticAlgorithm: This should not happen!');
    end
        
    %% Prealocation
    NewPopulation = arrayfun(@(x) zeros(Chromosome.Y, Chromosome.X), ...
        1:GAOpt.PopulationSize, 'UniformOutput', false);
    NewFitnessVal = zeros(GAOpt.PopulationSize,1);
    
    %% Elitism
    %  to avoid losing the best solution
    if GAOpt.Elitism == true
        [~,BestChromosome] = max(ScaledFitnessVal);
        NewPopulation{end} = Population{BestChromosome};
    end
        
    %% Crossover
    %  to give birth to offspring
    for j=1:GAOpt.PopulationSize-GAOpt.Elitism
        if rand(1)<=GAOpt.CrossoverProbability
            switch GAOpt.CrossoverType
                case 'one-point'
                    error('MTGeneticAlgorithm: Not implemented yet!');
                    
                case 'two-point'
                    error('MTGeneticAlgorithm: Not implemented yet!');
                    
                case 'uniform'
                    GensProbabilites = rand(Chromosome.X*Chromosome.Y, 1)*...
                        sum(ScaledFitnessVal([ParentsA(j) ParentsB(j)]));
                    GensFromA = GensProbabilites<=ScaledFitnessVal(ParentsA(j));
                    GensFromB = GensFromA==false;
                    NewPopulation{j}(GensFromA) = Population{ParentsA(j)}(GensFromA);
                    NewPopulation{j}(GensFromB) = Population{ParentsB(j)}(GensFromB);
                otherwise
                    error('MTGeneticAlgorithm: Unknown crossover type! Use one-point, two-point or uniform.');
            end
        else
            ParentProbability = rand(1)*sum(ScaledFitnessVal([ParentsA(j) ParentsB(j)]));
            if ParentProbability<=ScaledFitnessVal(ParentsA(j))
            	NewPopulation{j} = Population{ParentsA(j)};
            else
            	NewPopulation{j} = Population{ParentsB(j)};
            end
        end
    end
    
    %% Mutation to introduce new gens
    for j=1:GAOpt.PopulationSize-GAOpt.Elitism
        R = rand(Chromosome.X*Chromosome.Y,1);
        ToMutate = R<=GAOpt.MutationProbability;
        if ~isempty(FixedIndex)
            ToMutate(FixedIndex) = 0;
        end
        NewPopulation{j}(ToMutate) = 1-NewPopulation{j}(ToMutate);
    end
    
    %% Diversification
    if GAOpt.Diversity == true
        PopulationBin = false(GAOpt.PopulationSize, Chromosome.X*Chromosome.Y);
        for j=1:GAOpt.PopulationSize
            PopulationBin(j,:) = NewPopulation{j}(:);
        end
        [~, idx] = unique(PopulationBin, 'rows');
        NewPopulation = NewPopulation(idx);
        if length(NewPopulation) < GAOpt.PopulationSize
            for j=length(NewPopulation)+1:GAOpt.PopulationSize
                NewPopulation{j} = randi([0, 1], Chromosome.Y, ...
                    Chromosome.X);
                if ~isempty(FixedIndex)
                    NewPopulation{j}(FixedIndex) = GAOpt.FixVals(:,3);
                end
            end
        end
    end
    
    %% Fitness function evaluation
    % note that this operation can be easily parallelized
    if GAOpt.UseParallel
        spmd
            for j = labindex:GAOpt.NumWorkers:GAOpt.PopulationSize
                NewFitnessVal(j) = feval(FitnessFcn{labindex}, NewPopulation{j});
            end
        end
        NewFitnessVal = sum([NewFitnessVal{:}],2);
    else
        for j=1:GAOpt.PopulationSize
            NewFitnessVal(j) = feval(FitnessFcn, NewPopulation{j});
        end
    end
    
    % perform scaling & replace living population
    ScaledFitnessVal = fitness_scale(NewFitnessVal, GAOpt.FitnessScaleMin);
    FitnessVal = NewFitnessVal;
    Population = NewPopulation;
    
    %% Printing
    if strcmp(GAOpt.Display, 'on')
        fprintf('MTGeneticAlgorithm: Generation %d/%d:\tFitness = %f,\tmeanFit = %f.\n',...
            i, GAOpt.GenerationsCount, min(FitnessVal), mean(FitnessVal));
    end
    Misc.FitnessPath{i+1} = FitnessVal;
    Misc.Population{i+1} = Population;
    
end

if GAOpt.UseParallel
    delete(PoolObj);
end

[~,BestChromosomePos] = max(ScaledFitnessVal);
BestChromosome = Population{BestChromosomePos};
BestFitnessVal = FitnessVal(BestChromosomePos);

    % linearly scale fitness function to range [epsilon, 1]
    function ScaledFitnessVal = fitness_scale(FitnessVal, epsilon)
        if ~exist('epsilon','var')
            error('MTGeneticAlgorithm: This should not happen!');
        end
        
        if min(FitnessVal) == max(FitnessVal)
            ScaledFitnessVal = ones(size(FitnessVal));
        else
            ScaledFitnessVal = 1./(min(FitnessVal) - max(FitnessVal)) * ((1 - epsilon)*...
                FitnessVal + min(FitnessVal)*epsilon - max(FitnessVal));
        end
    end
    
    % plot binary population
    function PlotPopulation(Population, fname)
        figure;
        for ind=1:length(Population)
            subplot(length(Population), 1, ind)
            imagesc(Population{ind}(:)');
        end
        print([fname,'.pdf'], '-dpdf');
        close all;
    end

end
