%% Create simple beam FEMs according to specified parameters
%
% (c) Copyright 2016 Marek Tyburec, marek.tyburec@fsv.cvut.cz
%
%
% This file is part of Modular-Topology Optimization of Truss Structures Composed of Wang Tiles.
% 
% Modular-Topology Optimization of Truss Structures Composed of Wang Tiles
% is free software: you can redistribute it and/or modify it under the terms of the 
% GNU General Public License as published by the Free Software Foundation, 
% either version 3 of the License, or any later version.
% 
% Modular-Topology Optimization of Truss Structures Composed of Wang Tiles
% is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
% without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Modular-Topology Optimization of Truss Structures Composed of Wang Tiles.
% If not, see <http://www.gnu.org/licenses/>.
        
%% SETTINGS
% von Mises truss
% Width = 2; Height = 1;
% StackSize = [4 3];

TileType = 'bordernodes1';              % define type of used tiles (bordernodes1, bordernodes2, borderbars)
ScaleFac = 2;
Width = 8*ScaleFac;                     % set design domain width  \       rectangular
Height = 3*ScaleFac;                    % set design domain height /      design domain
SymmetricTiling = true;                 % enforce symmetry of the connectitity matrix (true/false)
TopoOptFormulation = 'socp';            % topology optimization formulation (sdp, socp, lp)
AssemblyOptFormulation = 'ga';          % assembly plan optimization formulation (sa, ga, enumeration)
StackSize = [1 1]/ScaleFac;             % size of a tile

%% DEFINITION OF WANG TILES
[Tiles, NNodes] = MTSetTileType(MTTILE, TileType, StackSize); % generate a vector of 16 wang tiles

%% DEFINE THE BEAM (hinge-supported with a force in the midspan)
BEAM = MTSTRUCT;                        % construct the beam object
MTAddTiles(BEAM, Tiles);                % assign tiles to the beam
BEAM.WIDTH = Width; BEAM.HEIGHT = Height;

% Define boundary conditions
switch TileType
    case 'borderbars'
        % External forces
        BEAM.IFY = [Width*StackSize(1)/2, ...
            Height*StackSize(2)-(-(NNodes-1)/(2*NNodes)+StackSize(2)/2)];
        BEAM.FY = -10;

        % Supports
        BEAM.IUX = [-(NNodes-1)/(2*NNodes)+StackSize(1)/2,...
            -(NNodes-1)/(2*NNodes)+StackSize(2)/2; ...
            Width*StackSize(1)-(-(NNodes-1)/(2*NNodes)+StackSize(2)/2),...
            -(NNodes-1)/(2*NNodes)+StackSize(2)/2]; 
        BEAM.UX = [0 0];
        BEAM.IUY = [-(NNodes-1)/(2*NNodes)+StackSize(1)/2,...
            -(NNodes-1)/(2*NNodes)+StackSize(2)/2; ...
            Width*StackSize(1)-(-(NNodes-1)/(2*NNodes)+StackSize(1)/2),...
            -(NNodes-1)/(2*NNodes)+StackSize(2)/2];
        BEAM.UY = [0 0];

    case 'bordernodes1'
        % External forces
        if mod(NNodes,2)    % if odd
            BEAM.IFY = [Width*StackSize(1)/2, Height*StackSize(2)];
            BEAM.FY = -10;
        else                % if even
            if mod(Width,2)
                BEAM.IFY = [Width*StackSize(1)/2, Height*StackSize(2)-...
                    StackSize(2)/(NNodes-1)/2];
            else
                BEAM.IFY = [Width*StackSize(1)/2, Height*StackSize(2)];
            end
            BEAM.FY = -10;
        end

        % Supports
        BEAM.IUX = [0 0; Width*StackSize(1) 0];
        BEAM.UX = [0 0];
        BEAM.IUY = [0 0; Width*StackSize(1) 0];
        BEAM.UY = [0 0];
        
    case 'bordernodes2'
        % External forces
        if mod(NNodes,2)    % if odd
            BEAM.IFY = [Width*StackSize(1)/2, Height*StackSize(2)];
            BEAM.FY = -10;
        else                % if even
            BEAM.IFY = [Width*StackSize(1)/2, ...
                Height*StackSize(2)-(-(NNodes-2)/(2*(NNodes-1))+StackSize(2)/2)];
            BEAM.FY = -10;
        end

        % Supports
        BEAM.IUX = [0 0; Width*StackSize(1) 0];
        BEAM.UX = [0 0];
        BEAM.IUY = [0 0; Width*StackSize(1) 0];
        BEAM.UY = [0 0];
end

%% TWO-LEVEL OPTIMIZATION
% for RepNumber = 1:50
RepNumber = 50;
rng(RepNumber);                                                             % for reproducibility of results

[BeamOpt, BeamIdeal, BeamWorst, Misc] = MTOptimizeTwoLevel(BEAM, ...
    TopoOptFormulation, AssemblyOptFormulation, SymmetricTiling);

save([AssemblyOptFormulation,num2str(RepNumber, '%02d'),'.mat']);
% end

%% PLOT RESULTS
% % worst-case design
% MTPlotTiles(BeamWorst, 'grouped');                                          % plot the worst-case tiled beam

% % ideal design
% MTPlotTiles(BeamIdeal, 'basic');                                            % plot the ideal beam

% % optimal solution
% MTPlotTiles(BeamOpt, 'grouped');                                            % plot the optimal result
% % optimal tile-set
% MTPlotTileSet(BeamOpt.TILESDEFINITION, 'full');
