% MTTILE class file
%
%   defines tile design used in two-phase optimization of trusses
%
% (c) Copyright 2016 Marek Tyburec, marek.tyburec@fsv.cvut.cz
%
%
% This file is part of Modular-Topology Optimization of Truss Structures Composed of Wang Tiles.
% 
% Modular-Topology Optimization of Truss Structures Composed of Wang Tiles
% is free software: you can redistribute it and/or modify it under the terms of the 
% GNU General Public License as published by the Free Software Foundation, 
% either version 3 of the License, or any later version.
% 
% Modular-Topology Optimization of Truss Structures Composed of Wang Tiles
% is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
% without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Modular-Topology Optimization of Truss Structures Composed of Wang Tiles.
% If not, see <http://www.gnu.org/licenses/>.

classdef MTTILE < handle
    properties
        AREAS                           % areas of individual bars
        ASSOCIATIONGROUPS               % association groups (tile-associated, edge-associated, vertex-associated)
        BARS                            % connection of nodes [node1, node2]
        BORDERNODES                     % nodes defining the shape of the tile [x,y]
        EDGESCOLOR = [0 .6 .9];         % color of tile edges
        FACECOLOR  = 'none';            % color of tile face
        GROUPS                          % groups assigned to individual bars
        INNERNODES                      % interior nodes of the tile [x,y]
        LENGTHS                         % bars' lengths
        MAXAREA = 100;                  % for plotting scale
        NUMBARS                         % total count of all possible bars
        NUMINNERNODES                   % number of inner nodes
        NUMNODES                        % number of all nodes
        OUTERNODES                      % outer nodes of the tile [x,y]
        STACKSIZE = [1 1];              % size of tile
        TEXTCOLOR = [.8 .8 .8];         % text color
        TILENUM                         % number of wang tile
    end
    methods
        function MTPlotTile(obj, x0, colormat, mode)
            % MTPlotTle function plots a tile
            %
            % ---INPUT-----------------------------------------------------
            %       obj             MTTILE class object
            %       x0              vector [x y] defining position of the
            %                         centroid of the tile, default [0 0]
            %       colormat        color matrix used for plotting
            %                         groups in different colors, can be empty
            %       mode            'full' for plotting of the shape, label
            %                         , Wang tiles and bars
            %                       'labeled' for plotting shape, Wang
            %                          tiles and label
            %                       'empty' for plotting only the shape
            %                       'inner' for plottting only the bars
            %                       'nodes' for plotting of nodes
            %                       'ground' for plotting the GS
            
            if ~exist('x0', 'var') || isempty(x0)
                x0 = [0 0];
            end
            if ~exist('colormat', 'var') || isempty(colormat)
                colormat = repmat([0 0 0],max(obj.GROUPS),1);
            end
            if ~exist('mode', 'var') || isempty(mode)
                mode = 'full';
            end
            
            % plot tile shape
            if strcmp(mode, 'full') || strcmp(mode, 'labeled') || strcmp(mode, 'empty')
                patch(obj.BORDERNODES(:,1)+x0(1), obj.BORDERNODES(:,2)+x0(2), 1, ...
                    'FaceColor', obj.FACECOLOR, 'EdgeColor', obj.EDGESCOLOR,...
                    'LineWidth', 1);
            end
            
            % plot tile name
            if strcmp(mode, 'full') || strcmp(mode, 'labeled')
%                 patch(x0(1)+obj.STACKSIZE(1)*[-1 1 1 -1]*.15, ...
%                       x0(2)+obj.STACKSIZE(2)*[-1 -1 1 1]*.15, ...
%                       1, 'FaceColor', 'none', 'EdgeColor', 'r', 'LineWidth', .5,...
%                       'LineStyle', '-.');
                text(x0(1), x0(2)-obj.STACKSIZE(2)/21, num2str(obj.TILENUM), ...
                    'HorizontalAlignment','center','VerticalAlignment','middle', ...
                    'FontUnits','points','FontSize',round(10*min(obj.STACKSIZE)),'Color', obj.TEXTCOLOR, ...
                    'Interpreter','Latex','FontName','Times');
            end
                
            % plot wang vertices
            if strcmp(mode, 'full') || strcmp(mode, 'labeled')
                BinaryNumbers = bin2dec(fliplr(dec2bin(obj.TILENUM-1,4))')';
%                 BinaryNumbers = de2bi(obj.TILENUM-1,4);     % convert tile number to binary representation (of vertices)
                ColorCodes = repmat([1 1 1],4,1);           % preallocate default colorcodes for '0'
                if sum(BinaryNumbers)>0                     % change if '1'
                    ColorCodes(BinaryNumbers>0,:) = repmat(obj.EDGESCOLOR,sum(BinaryNumbers),1);
                end
                WangNodes = [-1/2 1/2; -1/2 -1/2; 1/2 1/2; 1/2 -1/2].*repmat(obj.STACKSIZE,4,1) ...
                    + repmat(x0,4,1);                       % position of the patch
                
                Triangle = [0 0; 1 0; 0 1]/3*min(obj.STACKSIZE);    % triagle coordinates
                patch(WangNodes(1,1)+Triangle(:,1), WangNodes(1,2)-Triangle(:,2), 1, ...
                    'EdgeColor',obj.EDGESCOLOR, ...
                    'LineWidth',1,'FaceColor',ColorCodes(1,:));
                patch(WangNodes(2,1)+Triangle(:,1), WangNodes(2,2)+Triangle(:,2), 1, ...
                    'FaceVertexCData', repmat(obj.EDGESCOLOR,3,1), 'EdgeColor',obj.EDGESCOLOR, ...
                    'LineWidth',1,'FaceColor',ColorCodes(2,:));
                patch(WangNodes(3,1)-Triangle(:,1), WangNodes(3,2)-Triangle(:,2), 1, ...
                    'FaceVertexCData', repmat(obj.EDGESCOLOR,3,1), 'EdgeColor',obj.EDGESCOLOR, ...
                    'LineWidth',1,'FaceColor',ColorCodes(3,:));
                patch(WangNodes(4,1)-Triangle(:,1), WangNodes(4,2)+Triangle(:,2), 1, ...
                    'FaceVertexCData', repmat(obj.EDGESCOLOR,3,1),'EdgeColor',obj.EDGESCOLOR, ...
                    'LineWidth',1,'FaceColor',ColorCodes(4,:));
            end
                
            % plot bars
            if strcmp(mode, 'full') || strcmp(mode, 'inner') || strcmp(mode, 'ground')
                NODES = [obj.INNERNODES; obj.OUTERNODES];                   % get all nodes
                BARS_X = [NODES(obj.BARS(:,1),1) NODES(obj.BARS(:,2),1)]';  % x coordinates of bars
                BARS_Y = [NODES(obj.BARS(:,1),2) NODES(obj.BARS(:,2),2)]';  % y coordinates of bars

                if size(obj.OUTERNODES,2)                                   % if there exist helping nodes out of the tile
                    % Find coefficients of the line equation Ax + By + C = 0                                    
                    A = -diff(obj.BORDERNODES([1:end 1],2));
                    B = diff(obj.BORDERNODES([1:end 1],1));
                    C = obj.BORDERNODES([2:end 1],2).*obj.BORDERNODES(1:end,1) ...
                        - obj.BORDERNODES([2:end 1],1).*obj.BORDERNODES(1:end,2);

                    % Find distance t of the bars vector to tile edges
                    % A(px+tu1) + B(py+tu2) + C = 0
                    for i=1:length(A)
                        t = -(C(i)+A(i)*BARS_X(1,:)+B(i)*BARS_Y(1,:))./...
                            (A(i)*(BARS_X(2,:)-BARS_X(1,:))+B(i)*(BARS_Y(2,:)-BARS_Y(1,:)));
                        pos = (t>0)&(t<1);
                        
                        % modify nodes such that the bars do not cross edges
                        BARS_X(2,pos) = BARS_X(1,pos)+(BARS_X(2,pos)-BARS_X(1,pos)).*t(pos);
                        BARS_Y(2,pos) = BARS_Y(1,pos)+(BARS_Y(2,pos)-BARS_Y(1,pos)).*t(pos);
                    end
                end
                
                % if the areas of individual bars are not assigned, 1 is
                % the default to plot the GS
                if isempty(obj.AREAS) || strcmp(mode, 'ground')
                    plot(BARS_X+x0(1), BARS_Y+x0(2), 'LineWidth', 0.3, 'Color', 'k');
                    
                % otherwise plot based on AREAS
                else
                   colormat = colormat(obj.GROUPS,:);       % get group color
                   areas = obj.AREAS/obj.MAXAREA;           % normalize areas to 1

                   for i=1:size(BARS_X,2)                   % plot all bars
                       if areas(i)>5e-3
                           plot(BARS_X(:,i)+x0(1), BARS_Y(:,i)'+x0(2), 'LineWidth',...
                               3*sqrt(areas(i))*mean(obj.STACKSIZE), 'Color', colormat(i,:));
                       end
                   end
                end
            end    
            
            % if it is needed to display nodes
            if strcmp(mode,'nodes')
                line(obj.INNERNODES(:,1)+x0(1), obj.INNERNODES(:,2)+x0(2), ...
                  'LineStyle','none','Marker','o','MarkerEdge','b', ...
                  'MarkerFace',[.7 .7 .7], 'MarkerSize', 3);
            end
            
        end
        
        function MTDeleteInnerNodes(obj)
            % MTDeleteInnerNodes function deletes inner nodes from outer
            %   nodes
            %
            % ---INPUT-----------------------------------------------------
            %       obj             MTTILE class object
            
            OuterPosition = ismembertol(obj.OUTERNODES, obj.INNERNODES,...
                'ByRows', true)==false;                                     % find which nodes in outernodes are not in innernodes
            obj.OUTERNODES = obj.OUTERNODES(OuterPosition,:);               % sustain only the outer ones
            obj.NUMNODES = size(obj.INNERNODES,1)+size(obj.OUTERNODES,1);   % update nodes count
            obj.NUMINNERNODES = size(obj.INNERNODES,1);                     % set inner nodes count
        end
                
        function MTSetFullConnections(obj, MaxDistance)
            % MTSetFullConnections function creates fully connected GS with
            %   maximum bar length and eliminates overlapping bars
            %
            % ---INPUT-----------------------------------------------------
            %       obj             MTTILE class object
            %       MaxDistance     bars longer than MaxDistance are
            %                         removed
                        
            OldBars = obj.BARS;                                             % were the bars assigned?
            OldLengths = obj.LENGTHS;
            
            Nodes = [obj.INNERNODES; obj.OUTERNODES];                       % all nodes
            obj.BARS = combvec(1:size(obj.INNERNODES,1),1:size(Nodes,1))';  % create fully-connected GS
            obj.BARS = sort(obj.BARS,2);                                    % sort bars
            obj.BARS = uniquetol(obj.BARS, 'ByRows', true);                 % delete duplicates
            obj.BARS(obj.BARS(:,1)==obj.BARS(:,2),:) = [];                  % delete bars in the same node    
            
            % Delete long bars
            DeltaX = Nodes(obj.BARS(:,2),1)-Nodes(obj.BARS(:,1),1);         % difference in x coordinate
            DeltaY = Nodes(obj.BARS(:,2),2)-Nodes(obj.BARS(:,1),2);         % difference in y coordinate
            obj.LENGTHS = sqrt(DeltaX.^2 + DeltaY.^2);                      % compute lengths
            obj.BARS(obj.LENGTHS>MaxDistance+eps,:) = [];                   % delete long bars
            obj.LENGTHS(obj.LENGTHS>MaxDistance+eps) = [];                  % also delete lengths
            
            % Delete outer bars
            BARS_X = [Nodes(obj.BARS(:,1),1) Nodes(obj.BARS(:,2),1)]';      % x coordinates of bars
            BARS_X = BARS_X(:);
            BARS_Y = [Nodes(obj.BARS(:,1),2) Nodes(obj.BARS(:,2),2)]';      % y coordinates of bars
            BARS_Y = BARS_Y(:);
            [Inside, Boundary] = inpolygon(BARS_X, BARS_Y, ...
                obj.BORDERNODES([1:end 1],1), obj.BORDERNODES([1:end 1],2));% find bars inside and on boundary
            Outer = reshape(Inside==false, 2, [])';                         % get bars out
            Boundary = reshape(Boundary, 2, [])';                                       % get bars on boundary
            obj.BARS((sum(Outer|Boundary,2) == 2) & (sum(Boundary,2) ~= 2),:) = [];     % delete the outer bars
            obj.LENGTHS((sum(Outer|Boundary,2) == 2) & (sum(Boundary,2) ~= 2)) = [];
            
            % Make bars crossing boundaries shorter
            BARS_X = [Nodes(obj.BARS(:,1),1) Nodes(obj.BARS(:,2),1)]';      % x coordinates of bars
            BARS_Y = [Nodes(obj.BARS(:,1),2) Nodes(obj.BARS(:,2),2)]';      % y coordinates of bars
            A = -diff(obj.BORDERNODES([1:end 1],2));                        % line equation of boundaries: Ax + By + C = 0
            B = diff(obj.BORDERNODES([1:end 1],1));
            C = obj.BORDERNODES([2:end 1],2).*obj.BORDERNODES(1:end,1) ...
                - obj.BORDERNODES([2:end 1],1).*obj.BORDERNODES(1:end,2);
            for i=1:length(A)                                               % A(px+tu1) + B(py+tu2) + C = 0
                t = -(C(i)+A(i)*BARS_X(1,:)+B(i)*BARS_Y(1,:))./...
                    (A(i)*(BARS_X(2,:)-BARS_X(1,:))+B(i)*(BARS_Y(2,:)-BARS_Y(1,:))); % bar vector multiplicater to the boundaries
                CrossingBars = (t>0)&(t<1);
                BARS_X(2,CrossingBars) = BARS_X(1,CrossingBars)+...
                    (BARS_X(2,CrossingBars)-BARS_X(1,CrossingBars)).*t(CrossingBars);
                BARS_Y(2,CrossingBars) = BARS_Y(1,CrossingBars)+...
                    (BARS_Y(2,CrossingBars)-BARS_Y(1,CrossingBars)).*t(CrossingBars);
            end
            obj.LENGTHS = sqrt((BARS_X(2,:)-BARS_X(1,:)).^2 +...
                (BARS_Y(2,:)-BARS_Y(1,:)).^2);
            
            % Delete overlapping segments
            OverlappingBars = false(size(obj.BARS,1),1);
            ConnectedBars = arrayfun(@(x) find(sum(obj.BARS==x,2)>0), ...
                1:size(obj.INNERNODES,1), 'UniformOutput', false);          % find all connected bars to each node
            EndingNodes = arrayfun(@(x) obj.BARS(ConnectedBars{x},:), ...
                1:size(obj.INNERNODES,1), 'UniformOutput', false);          % find all nodes defining the bars
            EndingNodes = arrayfun(@(x) EndingNodes{x}(EndingNodes{x}~=x), ...
                1:size(obj.INNERNODES,1), 'UniformOutput', false);          % save only the nodes that are different to the fundamental node
            DeltaX = arrayfun(@(x) Nodes(x,1)-Nodes(EndingNodes{x},1), ...
                1:size(obj.INNERNODES,1), 'UniformOutput', false);          % difference in x direction
            DeltaY = arrayfun(@(x) Nodes(x,2)-Nodes(EndingNodes{x},2), ...
                1:size(obj.INNERNODES,1), 'UniformOutput', false);          % difference in y direction
            Lengths = arrayfun(@(x) sqrt(DeltaX{x}.^2+DeltaY{x}.^2), ...
                1:size(obj.INNERNODES,1), 'UniformOutput', false);          % get lengths of bars
            SinCos = arrayfun(@(x) [DeltaX{x}./Lengths{x} DeltaY{x}./Lengths{x}], ...
                1:size(obj.INNERNODES,1), 'UniformOutput', false);          % get geometric properties
            [~,~,ParallelBars] = arrayfun(@(x) uniquetol(SinCos{x}, 'ByRows', true),...
                1:size(obj.INNERNODES,1), 'UniformOutput', false);          % get parallel bars
            for i=1:size(obj.INNERNODES,1)
                for j=1:max(ParallelBars{i})
                    if sum(ParallelBars{i}==j)>1
                        BarsInDir = ParallelBars{i}==j;
                        ParallelLengths = Lengths{i}(BarsInDir);
                        PossibleRemoval = ConnectedBars{i}(BarsInDir);
                        OverlappingBars(PossibleRemoval(ParallelLengths~=min(ParallelLengths))) = true;
                    end
                end
            end
            obj.BARS(OverlappingBars,:) = [];
            obj.LENGTHS(OverlappingBars) = [];
            obj.NUMBARS = size(obj.BARS,1);
            
            % Add the new bars to old ones, if they exist
            if OldBars
                AddedInnerNodes = size(obj.INNERNODES,1)-obj.NUMINNERNODES; % NUMINNERNODES has not been touched yet
                OldBars(OldBars>obj.NUMINNERNODES) = ...
                    OldBars(OldBars>obj.NUMINNERNODES) + AddedInnerNodes;
                [obj.BARS, ind] = unique([OldBars; obj.BARS], 'rows');      % find duplicates
                obj.NUMINNERNODES = size(obj.INNERNODES,1);
                obj.NUMNODES = obj.NUMINNERNODES + size(obj.OUTERNODES,1);
                obj.LENGTHS = [OldLengths obj.LENGTHS];
                obj.LENGTHS = obj.LENGTHS(ind);
                obj.NUMBARS = size(obj.BARS,1);
            end
        end
        
        function MTAssignBarsAssociationGroups(obj)
            % MTAssignBarsAssociationGroups function creates wang group matrix based
            %   on bars' location related to edges and vertices
            %
            % ---INPUT-----------------------------------------------------
            %       obj             MTTILE class object [obj.BARS, obj.NUMBARS,
            %                       obj.NUMINNERNODES, obj.INNERNODES, 
            %                       obj.OUTERNODES, obj.TILENUM]
            
            obj.ASSOCIATIONGROUPS = zeros(obj.NUMBARS,1);                   % prealocation
            obj.GROUPS = zeros(obj.NUMBARS,1);                              % prealocation
            Nodes = [obj.INNERNODES; obj.OUTERNODES];                       % all nodes matrix
            BarsX = Nodes(obj.BARS(:,1),:);                                 % bar beginning coordinates
            BarsY = Nodes(obj.BARS(:,2),:);                                 % bar end coordinates
            
            % Vertex-associated bars [1 ... 0 vertice, 2 ... 1 vertice]
%             BinaryTileNumber = de2bi(obj.TILENUM-1,4);                      % binary tile number
%             bb = dec2bin(obj.TILENUM-1, 4);
            BinaryTileNumber = bin2dec(fliplr(dec2bin(obj.TILENUM-1,4))')';
            WangVertices = [-1/2 1/2; -1/2 -1/2; 1/2 1/2; 1/2 -1/2].*...
                repmat(obj.STACKSIZE,4,1);                                  % wang vertices coordinates
            for i=1:size(WangVertices,1)                                    % for each vertice
                t = (repmat(WangVertices(i,:),obj.NUMBARS,1)-BarsX)./...
                    (BarsY-BarsX);                                          
                t = (t(:,1)==t(:,2))&(t(:,1)<1&t(:,1)>0);                   % find which bars goes through vertice
                obj.ASSOCIATIONGROUPS(t) = BinaryTileNumber(i)+1;           % assign association number
            end
            
            % Edges-associated bars
            EdgesDefinition = [1 2; 3 4; 1 3; 2 4];                         % define edges order
            A = WangVertices(EdgesDefinition(:,2),2)-...
                WangVertices(EdgesDefinition(:,1),2);                       % Ax+By+C = 0
            B = WangVertices(EdgesDefinition(:,2),1)-...
                WangVertices(EdgesDefinition(:,1),1);                       % line equation of edges
            C = -A.*WangVertices(EdgesDefinition(:,1),1) - ...
                B.*WangVertices(EdgesDefinition(:,1),2);                    % C = - AX - BY
            for i=1:length(A)                                               % A(px+tu1) + B(py+tu2) + C = 0
                t = (-C(i) - A(i)*BarsX(:,1) - B(i)*BarsX(:,2))./ ...
                    (A(i)*(BarsY(:,1)-BarsX(:,1)) + B(i)*(BarsY(:,2)-BarsX(:,2)));     % t = ( -C -Apx - Bpy)./(Au1+Bu2)
                t = (t>10*eps)&(t<1-10*eps)&(obj.ASSOCIATIONGROUPS==0);     % get all bars for specific edge
                if abs(A(i)) <= eps
                    obj.ASSOCIATIONGROUPS(t) = ...
                        BinaryTileNumber(EdgesDefinition(i,:))*[1;2]+7;     % horizontal
                elseif abs(B(i)) <= 0
                    obj.ASSOCIATIONGROUPS(t) = ...
                        BinaryTileNumber(EdgesDefinition(i,:))*[1;2]+3;     % vertical
                else
                    error('MTTILE: Non-vertical and non-horizontal edge!'); % error
                end
            end
            
            % Tile-associated bars
            obj.ASSOCIATIONGROUPS(obj.ASSOCIATIONGROUPS==0) = 10 + obj.TILENUM; % all other bars are inner
            
        end
        
        function MTAssignBarsGroups(obj, mode)
            % MTAssignBarsGroups function creates final group matrix based
            % on ASSOCIATIONGROUPS vector
            %
            % ---INPUT-----------------------------------------------------
            %       obj             MTTILE class vector object
            %                       [obj.WANGGROUPS, obj.BARS, obj.INNERNODES,
            %                        obj.OUTERNODES]
            %       mode            defines how the groups should be
            %                       created, possible: 'wang', 'tiles', 'none'
            
            switch mode
                case 'wang'
                    AllAssociationGroups = {obj(:).ASSOCIATIONGROUPS};      % get all association groups
                    AllBars = {obj(:).BARS};                                % get all bars (from all tiles)
                    AllNodes = arrayfun(@(x) [obj(x).INNERNODES; ...
                        obj(x).OUTERNODES], ...
                        1:length(obj), 'UniformOutput', false);             % get all nodes
                    BarsCoordinates = arrayfun(@(x) ...
                        [AllNodes{x}(AllBars{x}(:,1),:), ...
                        AllNodes{x}(AllBars{x}(:,2),:)], 1:length(obj), ...
                        'UniformOutput', false);            
                    AssociationGroups = unique(vertcat(obj(:).ASSOCIATIONGROUPS)); % all assocation groups
                    GroupNumber = 1;                                        % assign the initial group number

                    for i=1:length(AssociationGroups)                       % for all association groups
                        WGroupInTile = find(cellfun(@(x) ...
                            any(ismember(AssociationGroups(i),x)),...
                            AllAssociationGroups));                         % find in which tiles the group i exists               
                        WhichBars = arrayfun(@(x) ...
                            find(AllAssociationGroups{WGroupInTile(x)}==AssociationGroups(i)), ...
                            1:length(WGroupInTile), 'UniformOutput', false);% find which bars belong to the group
                        WhichBarsCoors = arrayfun(@(x) ...
                            BarsCoordinates{x}(WhichBars{x},:), ...
                            1:length(WhichBars), 'UniformOutput', false);   % get belonging bars coordinates

                        % Vertex-associated bars
                        if AssociationGroups(i)<3                           % based on slope since all bars have to go through one point
                            Slope = arrayfun(@(x) (WhichBarsCoors{x}(:,3)-WhichBarsCoors{x}(:,1))./ ...
                                (WhichBarsCoors{x}(:,4)-WhichBarsCoors{x}(:,2)),1:length(WhichBarsCoors), ...
                                'UniformOutput',false);                     % compute slope
                            UniqueSlopes = uniquetol(vertcat(Slope{:}));    % get number of bar groups
                            for j=1:length(UniqueSlopes)                    % for all groups
                                for k=1:length(WGroupInTile)                % assign group number
                                    obj(WGroupInTile(k)).GROUPS(WhichBars{k}...
                                        (Slope{k}==UniqueSlopes(j))) = GroupNumber;
                                end                            
                                GroupNumber = GroupNumber+1;                % higher initial group number
                            end

                        % Vertical edge-associated bars
                        elseif AssociationGroups(i)<7                                      % vertical edge group
                            Negatives = arrayfun(@(x) [min(WhichBarsCoors{x}(:,[1 3]),[],2)<0, ...  
                                false(size(WhichBarsCoors{x},1),1), min(WhichBarsCoors{x}(:,[1 3]),[],2)<0,... % negative bars
                                false(size(WhichBarsCoors{x},1),1)], 1:length(WhichBarsCoors), 'UniformOutput', false);
                            WhichBarsCoors = arrayfun(@(x) WhichBarsCoors{x}+...    % move negative bars to positive
                                Negatives{x}*obj(WGroupInTile(x)).STACKSIZE(1), ...
                                1:length(WhichBarsCoors), 'UniformOutput', false);
                            ToSwap = arrayfun(@(x) WhichBarsCoors{x}(:,1)>WhichBarsCoors{x}(:,3),... % order bars coordinates
                                1:length(WhichBarsCoors), 'UniformOutput', false);
                            for j=1:length(ToSwap)
                                if sum(ToSwap{j})>0
                                    WhichBarsCoors{j}(ToSwap{j},:) = WhichBarsCoors{j}(ToSwap{j},[3 4 1 2]);
                                end
                            end
                            UniqueSlopes = uniquetol(vertcat(WhichBarsCoors{:}),'ByRows',true); % get unique bar groups
                            for j=1:size(UniqueSlopes,1)                                 % for all groups
                                for k=1:length(WGroupInTile)                        % assign group number                            
                                    obj(WGroupInTile(k)).GROUPS(WhichBars{k}...
                                        (ismembertol(WhichBarsCoors{k},...
                                        UniqueSlopes(j,:),'ByRows',true))) = GroupNumber;
                                end                            
                                GroupNumber = GroupNumber+1;                        % higher initial group number
                            end

                        % Horizontal edge-associated bars
                        elseif AssociationGroups(i)<11                                     % horizontal edge group
                            Negatives = arrayfun(@(x) [false(size(WhichBarsCoors{x},1),1), ... % negative bars
                                min(WhichBarsCoors{x}(:,[2 4]),[],2)<0, false(size(WhichBarsCoors{x},1),1), ...  
                                min(WhichBarsCoors{x}(:,[2 4]),[],2)<0 ], 1:length(WhichBarsCoors), 'UniformOutput', false);
                            WhichBarsCoors = arrayfun(@(x) WhichBarsCoors{x}+...    % move negative bars to positive
                                Negatives{x}*obj(WGroupInTile(x)).STACKSIZE(2), ...
                                1:length(WhichBarsCoors), 'UniformOutput', false);
                            ToSwap = arrayfun(@(x) WhichBarsCoors{x}(:,2)>WhichBarsCoors{x}(:,4),... % order bars coordinates
                                1:length(WhichBarsCoors), 'UniformOutput', false);
                            for j=1:length(ToSwap)
                                if sum(ToSwap{j})>0
                                    WhichBarsCoors{j}(ToSwap{j},:) = WhichBarsCoors{j}(ToSwap{j},[3 4 1 2]);
                                end
                            end
                            UniqueSlopes = uniquetol(vertcat(WhichBarsCoors{:}),'ByRows',true); % get unique bar groups
                            for j=1:size(UniqueSlopes,1)                                 % for all groups
                                for k=1:length(WGroupInTile)                        % assign group number                            
                                    obj(WGroupInTile(k)).GROUPS(WhichBars{k}...
                                        (ismembertol(WhichBarsCoors{k},...
                                        UniqueSlopes(j,:),'ByRows',true))) = GroupNumber;
                                end                            
                                GroupNumber = GroupNumber+1;                        % higher initial group number
                            end

                        % Tile-associated bars
                        else
                            obj(WGroupInTile).GROUPS(WhichBars{1}) = ...
                                GroupNumber:GroupNumber + length(WhichBars{1}) - 1;
                            GroupNumber = GroupNumber + length(WhichBars{1});
                        end
                    end

                case 'tiles'
                    % assign each tile unique groups
                    start = 1;
                    for i=1:length(obj)
                        obj(i).GROUPS(1:obj(i).NUMBARS,1) = ...
                            start:start+obj(i).NUMBARS-1;
                        start = start+obj(i).NUMBARS;
                    end
                    
                case 'none'
                    % do not assign groups
                    for i=1:length(obj)
                        obj(i).GROUPS = zeros(1,obj(i).NUMBARS);
                    end
            end
            
        end

        function new = MTCopy(obj, n)
            % MTCopy function copies the handle class
            %
            % ---INPUT-----------------------------------------------------
            %       obj             MTTILE class object
            %       n               how many times should be obj copied
            %
            % ---OUTPUT----------------------------------------------------
            %       new             MTTILE class object (vector)
            
            % Instantiate new object of the same class.
            if n==1
                new = feval(class(obj));
                p = properties(obj);                                        % copy all nonhidden properties
                for i = 1:length(p)
                    new.(p{i}) = obj.(p{i});
                end 
            elseif n>1
                p = properties(obj);                                        % copy all nonhidden properties
                for i=n:-1:1
                    new(i) = feval(class(obj));
                    for j=1:length(p)
                        new(i).(p{j}) = obj.(p{j});
                    end
                end 
            else
                error('n must be positive!');
            end    
        end
        
        function [Tiles, NNodes] = MTSetTileType(~,TileType, StackSize)
            % MTSetTileType function creates MTTILE vector object of
            %   specified tiles
            %
            % ---INPUT-----------------------------------------------------
            %       TileType        defines used tile, 'borderbars',
            %                         'bordernodes1', 'bordernodes2'
            %       StackSize       [width height] of a tile
            %
            % ---OUTPUT----------------------------------------------------
            %       Tiles           MTTILE class object (vector)
            
            switch TileType
                case 'borderbars'
                    NNodes = 3; MaxLength = sqrt(2*(1/NNodes)^2);
                    Tiles(1) = MTTILE;
                    Tiles(1).BORDERNODES = [-0.5 -0.5; 0.5 -0.5; 0.5 0.5; -0.5 0.5];
                    Tiles(1).INNERNODES = combvec(-(NNodes-1)/(2*NNodes):1/NNodes:(NNodes-1)/(2*NNodes), ...
                                                     -(NNodes-1)/(2*NNodes):1/NNodes:(NNodes-1)/(2*NNodes))';
                    Tiles(1).OUTERNODES = combvec(-(NNodes+1)/(2*NNodes):1/NNodes:(NNodes+1)/(2*NNodes), ...
                                                     -(NNodes+1)/(2*NNodes):1/NNodes:(NNodes+1)/(2*NNodes))';
                    MTDeleteInnerNodes(Tiles(1));
                    MTSetFullConnections(Tiles(1), MaxLength);

                case 'bordernodes1'
                    NNodes = 4; MaxLength = 1/(NNodes-1);
                    Tiles(1) = MTTILE;
                    Tiles(1).BORDERNODES = [-0.5 -0.5; 0.5 -0.5; 0.5 0.5; -0.5 0.5];
                    Tiles(1).INNERNODES = combvec(linspace(-0.5+1/((NNodes-1)*2), 0.5-1/((NNodes-1)*2), NNodes-1), ...
                        linspace(-0.5+1/((NNodes-1)*2), 0.5-1/((NNodes-1)*2), NNodes-1))';
                    Tiles(1).OUTERNODES = combvec(linspace(-0.5-1/((NNodes-1)*2),...
                        0.5+1/((NNodes-1)*2), NNodes+1), linspace(-0.5-1/((NNodes-1)*2), ...
                        0.5+1/((NNodes-1)*2), NNodes+1))';
                    MTDeleteInnerNodes(Tiles(1));
                    MTSetFullConnections(Tiles(1), MaxLength);
                    Tiles(1).INNERNODES = [Tiles(1).INNERNODES; combvec(linspace(-0.5, 0.5, NNodes), ...
                        linspace(-0.5, 0.5, NNodes))'];
                    MaxLength = 0.5*sqrt(2)/(NNodes-1);
                    MTSetFullConnections(Tiles(1), MaxLength);

                case 'bordernodes2'
                    NNodes = 4; MaxLength = 1/(NNodes-1)+1e-8;
                    Tiles(1) = MTTILE;
                    Tiles(1).BORDERNODES = [-0.5 -0.5; 0.5 -0.5; 0.5 0.5; -0.5 0.5];
                    Tiles(1).INNERNODES = [combvec(linspace(-0.5, 0.5, NNodes), ...
                        linspace(-0.5, 0.5, NNodes))'; ...
                        combvec(linspace(-0.5+1/((NNodes-1)*2), 0.5-1/((NNodes-1)*2), NNodes-1), ...
                        linspace(-0.5+1/((NNodes-1)*2), 0.5-1/((NNodes-1)*2), NNodes-1))'];
                    Tiles(1).OUTERNODES = combvec(linspace(-0.5-1/((NNodes-1)*2),...
                        0.5+1/((NNodes-1)*2), NNodes+1), linspace(-0.5-1/((NNodes-1)*2), ...
                        0.5+1/((NNodes-1)*2), NNodes+1))';
                    MTDeleteInnerNodes(Tiles(1));
                    MTSetFullConnections(Tiles(1), MaxLength);

                otherwise
                    error('MTTILE: Unknown tile type!');
            end
            
            % scale to StackSize
            Tiles(1).INNERNODES(:,1) = Tiles(1).INNERNODES(:,1)*StackSize(1);
            Tiles(1).INNERNODES(:,2) = Tiles(1).INNERNODES(:,2)*StackSize(2);
            Tiles(1).OUTERNODES(:,1) = Tiles(1).OUTERNODES(:,1)*StackSize(1);
            Tiles(1).OUTERNODES(:,2) = Tiles(1).OUTERNODES(:,2)*StackSize(2);
            Tiles(1).BORDERNODES(:,1) = Tiles(1).BORDERNODES(:,1)*StackSize(1);
            Tiles(1).BORDERNODES(:,2) = Tiles(1).BORDERNODES(:,2)*StackSize(2);
            Tiles(1).STACKSIZE = StackSize;
            if StackSize(1)==StackSize(2)
                Tiles(1).LENGTHS = Tiles(1).LENGTHS*StackSize(1);
            else
                Nodes = [Tiles(1).INNERNODES; Tiles(1).OUTERNODES];
                DeltaX = Nodes(Tiles(1).BARS(:,2),1)-Nodes(Tiles(1).BARS(:,1),1);         % difference in x coordinate
                DeltaY = Nodes(Tiles(1).BARS(:,2),2)-Nodes(Tiles(1).BARS(:,1),2);         % difference in y coordinate
                Tiles(1).LENGTHS = sqrt(DeltaX.^2 + DeltaY.^2);                      % update lengths

                %error('MTTILE: Non-square shape is currently not supported!');
            end
            
            Tiles = MTCopy(Tiles(1), 16);
            for i=1:length(Tiles)
                Tiles(i).TILENUM = i;
                MTAssignBarsAssociationGroups(Tiles(i));
            end
            MTAssignBarsGroups(Tiles, 'wang');
                        
        end
        
        function MTPlotTileSet(Obj, Mode)
            % MTSetTileType function plots tile set based on input
            % objective. If the obj. is MTTILE vector, initial tileset is
            % plotted. If the obj is MTSTRUCT, final tileset is plotted.
            %
            % ---INPUT-----------------------------------------------------
            %       Obj                     MTTILE class vector
            %       Type                    mode for MTPlotTile
            
            axis equal tight off;
            hold on;
            for i=1:length(Obj)
                MTPlotTile(Obj(i), [mod(i-1,8)*Obj(i).STACKSIZE(1)+Obj(i).STACKSIZE(1),...
                    -(i-1-mod(i-1,8))/8*Obj(i).STACKSIZE(2)+Obj(i).STACKSIZE(2)]*1.5, [], Mode);
            end
            hold off;
                        
            texts = findall(gcf, 'Type', 'text');
            FontSizes = get(texts, 'FontSize');
            FontSizes = cellfun(@(x) x/max(Obj(1).STACKSIZE), FontSizes, 'un', 0);
            set(texts, {'FontSize'}, FontSizes);
            
            % lines
            lines = findall(gcf, 'Type', 'line', 'Color','k');
            LineWidths = get(lines, 'LineWidth');
            LineWidths = cellfun(@(x) x/max(Obj(1).STACKSIZE), LineWidths, 'un', 0);
            set(lines, {'LineWidth'}, LineWidths);
            
            XLim = xlim();
            xlim(XLim);
            YLim = ylim();
            Width = 8.4;
            Multiplicator = (XLim(2)-XLim(1))/Width;
            Height = (YLim(2)-YLim(1))/Multiplicator;
            set(gca, 'position', [0.01 0 1-0.02 1], 'units', 'normalized');
            set(gcf, 'PaperUnits', 'centimeters', 'PaperPositionMode', 'auto',...
                'PaperPosition', [0 0 Width Height], 'PaperSize', [Width, Height]);            
        end
        
        function MTExportTileSetTxt(Obj, fname)
            % MTExportTileSetTxt function exports tile set.
            %
            % ---INPUT-----------------------------------------------------
            %       obj                     MTTILE class vector
            %       fname                   file name
            
            fid = fopen(fname, 'w+');
            
            for tile=1:length(Obj)
                obj = Obj(tile);
                
                fprintf(fid, '# TILE %d\n', tile);
                
                NODES = [obj.INNERNODES; obj.OUTERNODES];                   % get all nodes
                BARS_X = [NODES(obj.BARS(:,1),1) NODES(obj.BARS(:,2),1)]';  % x coordinates of bars
                BARS_Y = [NODES(obj.BARS(:,1),2) NODES(obj.BARS(:,2),2)]';  % y coordinates of bars

                if size(obj.OUTERNODES,2)                                   % if there exist helping nodes out of the tile
                    % Find coefficients of the line equation Ax + By + C = 0                                    
                    A = -diff(obj.BORDERNODES([1:end 1],2));
                    B = diff(obj.BORDERNODES([1:end 1],1));
                    C = obj.BORDERNODES([2:end 1],2).*obj.BORDERNODES(1:end,1) ...
                        - obj.BORDERNODES([2:end 1],1).*obj.BORDERNODES(1:end,2);

                    % Find distance t of the bars vector to tile edges
                    % A(px+tu1) + B(py+tu2) + C = 0
                    for i=1:length(A)
                        t = -(C(i)+A(i)*BARS_X(1,:)+B(i)*BARS_Y(1,:))./...
                            (A(i)*(BARS_X(2,:)-BARS_X(1,:))+B(i)*(BARS_Y(2,:)-BARS_Y(1,:)));
                        pos = (t>0)&(t<1);

                        % modify nodes such that the bars do not cross edges
                        BARS_X(2,pos) = BARS_X(1,pos)+(BARS_X(2,pos)-BARS_X(1,pos)).*t(pos);
                        BARS_Y(2,pos) = BARS_Y(1,pos)+(BARS_Y(2,pos)-BARS_Y(1,pos)).*t(pos);
                    end
                end
                
                areas = obj.AREAS/obj.MAXAREA;
                
%                 hold on;
                for bar=1:size(BARS_X,2)
                    if (areas(bar)> 5e-3)
                        fprintf(fid, 'x1=%f y1=%f x2=%f y2=%f a=%f\n', ...
                            BARS_X(1,bar), BARS_Y(1,bar), BARS_X(2,bar), BARS_Y(2,bar), obj.AREAS(bar));
%                         plot([BARS_X(1,bar) BARS_X(2,bar)], [BARS_Y(1,bar) BARS_Y(2,bar)], 'Color', 'k', 'LineWidth',...
%                                    3*sqrt(areas(bar))*mean(obj.STACKSIZE));
                
                    end
                end
                
                fprintf(fid, '\n');
            end
        end
        
        function MTExportTileSetStl(Obj)
            % MTExportTileSetStl function exports tile set to stl format
            %
            % ---INPUT-----------------------------------------------------
            %       obj                     MTTILE class vector
            
            tileVert = cell(length(Obj));
            tileFac = cell(length(Obj));
            for tile=1:length(Obj)
                obj = Obj(tile);
                NODES = [obj.INNERNODES; obj.OUTERNODES]+obj.STACKSIZE(1)/2;
                NodesBars = [0.1*ones(size(NODES,1),1), NODES];
                NodesBars = [NodesBars; 0.4 0 0];
                Bars = obj.BARS;
                CrossSections = (obj.AREAS/obj.MAXAREA).^(1/3)/150;
                BoundaryBarsL = false(size(Bars,1),1);
                deltas = [0.4 0.4 0.4];
                OuterDimensions = [0.4 obj.STACKSIZE(1) obj.STACKSIZE(2)];
                MinCrossSectionCutOff = 1e-3;
                BoundaryShells = true;
                BoundaryShellThickness = 0.05;
                Handles = false;
                stlName = [num2str(tile), 'b.stl'];
                [tileVert{tile}, tileFac{tile}] = tiles2stl(NodesBars, Bars, BoundaryBarsL, CrossSections, deltas, OuterDimensions, MinCrossSectionCutOff, BoundaryShells, BoundaryShellThickness, Handles, stlName);
            end
        end
        
        function MTExportTileSetJson(Obj)
            % MTExportTileSetTxt function exports tile set.
            %
            % ---INPUT-----------------------------------------------------
            %       obj                     MTTILE class vector
            
            for tile=1:length(Obj)
                
                fid = fopen([num2str(tile), '.json'], 'w+');
                
                obj = Obj(tile);
                
                % create structure with json parameters
                jsonStruct.bounds.x.min = -obj.STACKSIZE(1)/2;
                jsonStruct.bounds.x.max = obj.STACKSIZE(1)/2;
                jsonStruct.bounds.y.min = -obj.STACKSIZE(1)/2;
                jsonStruct.bounds.y.max = obj.STACKSIZE(1)/2;
                jsonStruct.bounds.z.min = 0;
                jsonStruct.bounds.z.max = 0.2;
                                
                NODES = [obj.INNERNODES; obj.OUTERNODES];                   % get all nodes
                
                % initialize nodes structure
                nodesStruct = cell(size(NODES,1),1);
                
                for i=1:size(NODES,1)
                    nodesStruct{i}.x = NODES(i,1);
                    nodesStruct{i}.y = NODES(i,2);
                    nodesStruct{i}.z = 0.0;
                end
                
                areas = obj.AREAS/obj.MAXAREA;
                areasNnz = find(areas > 5e-3);
                barsStruct = cell(length(areasNnz),1);
                
                rectangle.width = 0.0;
                rectangle.height = 0.2;
                rectangle.alignHeight = 'z';
                
                for i=1:length(areasNnz)
                    barsStruct{i}.startNode = obj.BARS(areasNnz(i),1)-1;
                    barsStruct{i}.endNode = obj.BARS(areasNnz(i),2)-1;
                    barsStruct{i}.crossSectionShape = 'rectangle';
                    barsStruct{i}.crossSectionData = rectangle;
                    barsStruct{i}.crossSectionData.width = obj.AREAS(areasNnz(i)) / rectangle.height / 30;                    
                end
                
                jsonStruct.nodes = nodesStruct;
                jsonStruct.bars = barsStruct;
                
                jsonText = jsonencode(jsonStruct);
                
                fprintf(fid, jsonText);
                
                fclose(fid);
            end
        end
    end
end
