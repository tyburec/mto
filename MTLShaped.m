%% Create simple beam FEMs according to specified parameters
%
% (c) Copyright 2016 Marek Tyburec, marek.tyburec@fsv.cvut.cz
%
%
% This file is part of Modular-Topology Optimization of Truss Structures Composed of Wang Tiles.
% 
% Modular-Topology Optimization of Truss Structures Composed of Wang Tiles
% is free software: you can redistribute it and/or modify it under the terms of the 
% GNU General Public License as published by the Free Software Foundation, 
% either version 3 of the License, or any later version.
% 
% Modular-Topology Optimization of Truss Structures Composed of Wang Tiles
% is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
% without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Modular-Topology Optimization of Truss Structures Composed of Wang Tiles.
% If not, see <http://www.gnu.org/licenses/>.

%% SETTINGS
TileType = 'bordernodes1';              % define type of used tiles (bordernodes1, bordernodes2, borderbars)
ScaleFac = 2;
Width = 6*ScaleFac;                     % set design domain width  \       rectangular
Height = 6*ScaleFac;                    % set design domain height /      design domain
SymmetricTiling = false;                 % enforce symmetry of the connectitity matrix (true/false)
TopoOptFormulation = 'socp';            % topology optimization formulation (sdp, socp, lp)
AssemblyOptFormulation = 'ga';          % assembly plan optimization formulation (sa, ga, enumeration)
StackSize = [1 1]/ScaleFac;             % size of a tile
Holes = false(Height+1, Width+1);
Holes(1:ceil(0.5*Height-1e-3), 1+ceil((0.5+1e-3)*Width):Width+1) = true;

%% DEFINITION OF WANG TILES
[Tiles, NNodes] = MTSetTileType(MTTILE, TileType, StackSize); % generate a vector of 16 wang tiles

%% DEFINE THE BEAM (hinge-supported with a force in the midspan)
BEAM = MTSTRUCT;                        % construct the beam object
MTAddTiles(BEAM, Tiles);                % assign tiles to the beam
BEAM.WIDTH = Width; BEAM.HEIGHT = Height;
BEAM.PRESCRIBEDVERTICES = Holes;
BEAM.PRESCRIBEDVERTICESVALUES = changem(double(Holes), nan, 1);

% optimization parameters
BEAM.VOLBOUND = 100;
BEAM.STRESSBOUNDTENSION = 20;
BEAM.STRESSBOUNDCOMPRESSION = -20;

% Define boundary conditions
% External forces
BEAM.IFY = [Width*StackSize(1), Height*StackSize(2)/4 1]; 
BEAM.IFX = [Width*StackSize(1), Height*StackSize(2)/4 2];
BEAM.FY = -10;
BEAM.FX = 1;

% Supports
hx = 0:1/3*StackSize(1):Width*StackSize(1)/2;
BEAM.IUX = [hx' ones(size(hx'))*Height*StackSize(2)];
BEAM.UX = zeros(size(BEAM.UX,1),1);
BEAM.IUY = BEAM.IUX;
BEAM.UY = BEAM.UX;

%% TWO-LEVEL OPTIMIZATION
% for RepNumber = 1:50
RepNumber = 1;
rng(RepNumber);                                                             % for reproducibility of results

[BeamOpt, BeamIdeal, BeamWorst, Misc] = MTOptimizeTwoLevel(BEAM, ...
    TopoOptFormulation, AssemblyOptFormulation, SymmetricTiling);

save([AssemblyOptFormulation,num2str(RepNumber, '%02d'),'.mat']);
% end

%% PLOT RESULTS
% % worst-case design
% MTPlotTiles(BeamWorst, 'grouped');                                          % plot the worst-case tiled beam

% % ideal design
% MTPlotTiles(BeamIdeal, 'basic');                                            % plot the ideal beam

% % optimal solution
% MTPlotTiles(BeamOpt, 'grouped');                                            % plot the optimal result
% % optimal tile-set
% MTPlotTileSet(BeamOpt.TILESDEFINITION, 'full');
