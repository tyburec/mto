function [Omega, E, Misc] = MTSimulatedAnnealing(Func, Omega0, T0, NMax, NSteps, TFunc, NFunc, SAOpt)
%MTSIMULATEDANNEALING Custom simulated annealing implementation
%
%  Based on:
%    Salamon, P., Sibani, P., & Frost, R. (2002). Facts, conjectures,
%       and improvements for simulated annealing. Siam.
%
%   INPUT:
%           Func        evaluated objective function handle
%           Omega0      initial design variables guess
%           T0          initial temperature
%           NMax        number of temperatures
%           NSteps      number of steps for each temperature to reach
%                       equilibrium
%           TFunc       cooling function (with previous temp. as input parameter)
%           NFunc       function to select neighbor (with Omega, Iter and IMax inputs)
%           SAOpt       SAOpt structure
%
%   OUTPUT:
%           Omega       optimal design variables
%           E           optimal objective function value
%           Misc        information about convergance of algorithm
%
% (c) Copyright 2016 Marek Tyburec, marek.tyburec@fsv.cvut.cz
%
%
% This file is part of Modular-Topology Optimization of Truss Structures Composed of Wang Tiles.
% 
% Modular-Topology Optimization of Truss Structures Composed of Wang Tiles
% is free software: you can redistribute it and/or modify it under the terms of the 
% GNU General Public License as published by the Free Software Foundation, 
% either version 3 of the License, or any later version.
% 
% Modular-Topology Optimization of Truss Structures Composed of Wang Tiles
% is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
% without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Modular-Topology Optimization of Truss Structures Composed of Wang Tiles.
% If not, see <http://www.gnu.org/licenses/>.

%% Check inputs
if ~exist('Func', 'var')
    error('MTSimulatedAnnealing: Func needs to be specified.');
end
if (numel(Func) == 1) && ~isa(Func, 'function_handle')
    error('MTSimulatedAnnealing: Func needs to be a function handle.');
end

if ~exist('Omega0', 'var')
    error('MTSimulatedAnnealing: Omega0 needs to be specified.');
end

if ~exist('T0', 'var')
    error('MTSimulatedAnnealing: T0 needs to be specified.');
elseif length(T0(:))~=1
    error('MTSimulatedAnnealing: T0 needs to be assigned a single number.');
elseif (T0 <= 0)
    error('MTSimulatedAnnealing: T0 needs to be assigned a positive value.');
end

if ~exist('NMax', 'var')
    error('MTSimulatedAnnealing: NMax needs to be specified.');
elseif length(NMax(:))~=1
    error('MTSimulatedAnnealing: NMax needs to be assigned a single number.');
elseif (NMax <= 0)
    error('MTSimulatedAnnealing: NMax needs to be assigned a positive value.');
end

if ~exist('NSteps', 'var')
    error('MTSimulatedAnnealing: NSteps needs to be specified.');
elseif length(NSteps(:))~=1
    error('MTSimulatedAnnealing: NSteps needs to be assigned a single number.');
elseif (NSteps <= 0)
    error('MTSimulatedAnnealing: NSteps needs to be assigned a positive value.');
end

if ~exist('TFunc', 'var')
    error('MTSimulatedAnnealing: TFunc needs to be specified.');
end
if (numel(TFunc) == 1) && ~isa(TFunc, 'function_handle')
    error('MTSimulatedAnnealing: TFunc needs to be a function handle.');
end

if ~exist('NFunc', 'var')
    error('MTSimulatedAnnealing: TFunc needs to be specified.');
end
if (numel(NFunc) == 1) && ~isa(NFunc, 'function_handle')
    error('MTSimulatedAnnealing: NFunc needs to be a function handle.');
end

if ~exist('SAOpt', 'var')
    SAOpt = struct();
end

if ~isfield(SAOpt, 'Display')
    SAOpt.Display = 'on';
elseif ~ischar(SAOpt.Display)
    error('MTSimulatedAnnealing: SAOpt.Display needs to be assigned an array of chars.');
elseif ~strcmp(SAOpt.Display, 'on') || ~strcmp(SAOpt.Display, 'off')
    error('MTSimulatedAnnealing: SAOpt.Display can be set either on or off.');
end

if ~isfield(SAOpt, 'FixVals')
    SAOpt.FixVals = [];
end

if strcmp(SAOpt.Display, 'on')
    fprintf('MTSimulatedAnnealing: starting optimization...');
end

%% Initialize = initial parameter set
Omega = Omega0;                                     % initialize state

% fix requested values
if isempty(SAOpt.FixVals)
    FixedIndex = [];
else
    FixedIndex = sub2ind(size(Omega), SAOpt.FixVals(:,2), SAOpt.FixVals(:,1));
    Omega(FixedIndex) = SAOpt.FixVals(:,3);
end

T = T0;                                             % initialize temperature
E = feval(Func, Omega);                             % initialize energy
Misc.EnergyPath = zeros(NMax*NSteps+1,1);           % here will be saved the current energy
Misc.EnergyPath(1) = E;                             % save the initial energy
Misc.EnergyTrial = zeros(NMax*NSteps+1,1);
Misc.EnergyTrial(1) = E;

if strcmp(SAOpt.Display, 'on')
    fprintf('MTSimulatedAnnealing: Iter %d/%d:\tE = %f.\n',...
        0, NSteps*NMax , E);
end

%% Evaluate solution
for i = 1:NMax                                      % limit number of temperatures
    for j=1:NSteps                                  % number of steps for each temperature to reach equilibrium
        if ~isempty(FixedIndex)
            Omega(FixedIndex) = NaN;
        end
        TrialOmega = feval(NFunc, Omega,...
            (i-1)*NSteps+j+1, NMax*NSteps);         % randomly select new neighbor
        if ~isempty(FixedIndex)
            Omega(isnan(Omega)) = SAOpt.FixVals(:,3);
            TrialOmega(isnan(TrialOmega)) = SAOpt.FixVals(:,3);
        end
        E_new = feval(Func, TrialOmega);            % compute new solution energy
        DeltaE = E_new - E;                         % difference between energies

        if DeltaE<=0                                % if the solution is better, it is always accepted
            Omega = TrialOmega;                     % update solution
            E = E_new;                              % update energy
        else                                        
            if rand(1)<exp(-DeltaE/T)               % if not, it is accepted only based on probability
                Omega = TrialOmega;                 % update solution
                E = E_new;                          % update energy
            end
        end
        
        Misc.EnergyPath((i-1)*NSteps+j+1) = E;
        Misc.EnergyTrial((i-1)*NSteps+j+1) = E_new;
        
        if strcmp(SAOpt.Display, 'on')
            fprintf('MTSimulatedAnnealing: Iter %d/%d:\tE = %f,\tEtrial = %f.\n',...
                (i-1)*NSteps+j, NSteps*NMax , E, E_new);
        end
    end
    T = feval(TFunc, T);                            % system cooling
    
end 
end
