% MTSTRUCT class file
%
%   defines design domain used in two-phase optimization of trusses
%
% (c) Copyright 2016 Marek Tyburec, marek.tyburec@fsv.cvut.cz
%
%
% This file is part of Modular-Topology Optimization of Truss Structures Composed of Wang Tiles.
% 
% Modular-Topology Optimization of Truss Structures Composed of Wang Tiles
% is free software: you can redistribute it and/or modify it under the terms of the 
% GNU General Public License as published by the Free Software Foundation, 
% either version 3 of the License, or any later version.
% 
% Modular-Topology Optimization of Truss Structures Composed of Wang Tiles
% is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
% without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Modular-Topology Optimization of Truss Structures Composed of Wang Tiles.
% If not, see <http://www.gnu.org/licenses/>.

classdef MTSTRUCT < handle
    properties
        A                               % cross sections
        BARS                            % bars of the global structure
        BARSGROUPS                      % bars divided into groups
        BARSLENGTHS                     % length of bars
        CONNECTIVITYMATRIX              % connectivity matrix
        E = 1;                          % Young's modulus
        FORCEVECTOR                     % external nodal force vector
        FX                              % external nodal force fx
        FY                              % external nodal force fy
        GROUPMATRIX                     % group matrix
        HEIGHT                          % height of the design domain
        IFX                             % external nodal force fx placement [x,y]
        IFY                             % external nodal force fy placement [x,y]
        IUX                             % x-direction support placement [x,y]
        IUY                             % y-direction support placement [x,y]
        MINAREA = 1e-6;                 % lower area is treated as zero
        NODES                           % nodes of the global structure
        NUMBARS                         % total count of bars
        NUMGROUPS                       % total count of groups
        NUMLC                           % number of load cases
        NUMNODES                        % total count of nodes
        OBJECTIVE                       % optimized objective value
        PRESCRIBEDVERTICES = [];        % positions of fixed vertices
        PRESCRIBEDVERTICESVALUES = [];  % fixed vertices values
        STATICMATRIX                    % static matrix A
        STRESSBOUNDTENSION = Inf;       % stress bound for the bars in tension
        STRESSBOUNDCOMPRESSION = Inf;   % stress bound for the bars in compression
        TILESDEFINITION                 % cell of MTTILEs
        TILESNUMBERS                    % matrix defining arragement of tiles (eg. 1, 0, 2)
        TILESNODES                      % nodes in each tile
        UX                              % predefined x-direction displacements
        UY                              % predefined y-direction displacements
        VOLBOUND = 100;                 % volume upper bound
        WIDTH                           % width of the design domain
        XFORNODES                       % nodes with fx
        XSUPNODES                       % nodes with x-direction supports
        YFORNODES                       % nodes with fy
        YSUPNODES                       % nodes with y-direction supports
    end
    
    methods
        
        function MTCreateGroundStructure(Obj)
            % MTCreateGroundStructure function creates the GS
            %
            % ---INPUT-----------------------------------------------------
            %       Obj                 MTSTRUCT class object

            [Width, Height] = size(Obj.TILESNUMBERS);                       % get structural domain dimensions
            Obj.TILESNODES = cell(Width, Height);                           % preallocate cell for nodes of all tiles
            Obj.NODES = zeros(0,2);                                         % preallocation for nodes defining the GS
            NodeInTiles = zeros(Obj.TILESDEFINITION(1).NUMNODES*...
                nnz(Obj.TILESNUMBERS(:)),2);                                % preallocate matrix for each node
            LastNum = 0;                                                    % last added node number
            
            % loop through the whole structural design domain
            for i=1:Width
                for j=1:Height
                    TileNumber = Obj.TILESNUMBERS(i,j);                     % get tile number on current position
                    if TileNumber>0                                         % if there is a tile
                        StackSize = Obj.TILESDEFINITION(TileNumber).STACKSIZE;  % tile size
                        
                        % if the tile has edge-associated bars
                        if size(Obj.TILESDEFINITION(TileNumber).OUTERNODES,1)
                            
                            % get all nodes of the tile (inner, outer) moved to their proper locations
                            Obj.TILESNODES{i,j} = [Obj.TILESDEFINITION(TileNumber).INNERNODES; ...
                                Obj.TILESDEFINITION(TileNumber).OUTERNODES] + ...
                                repmat([StackSize(1)/2+StackSize(1)*(i-1) ...
                                       StackSize(2)/2+StackSize(2)*(j-1)],...
                                       Obj.TILESDEFINITION(TileNumber).NUMNODES,1);
                            
                        % if the tile does not have edge-associated bars
                        else
                            
                            % get all nodes of the tile (inner) moved to their proper locations
                            Obj.TILESNODES{i,j} = [Obj.TILESDEFINITION(TileNumber).INNERNODES] + ...
                                repmat([StackSize(1)/2+StackSize(1)*(i-1) ...
                                        StackSize(2)/2+StackSize(2)*(j-1)],...
                                        Obj.TILESDEFINITION(TileNumber).NUMNODES,1);
                        end
                        
                        % add inner nodes to the list of all nodes
                        Obj.NODES = [Obj.NODES; ...
                            Obj.TILESNODES{i,j}(1:size(Obj.TILESDEFINITION(TileNumber).INNERNODES,1),:)];

                        % mark in which tile are the nodes
                        TileMark = repmat([i j],size(Obj.TILESDEFINITION(TileNumber).INNERNODES,1),1);    % all the nodes are in the same tile
                        NodeInTiles(LastNum+1:LastNum+size(TileMark,1),:) = TileMark;
                        LastNum = LastNum+length(TileMark);
                    end
                end
            end
            
            % If the node is placed on a boundary, it might belong to multiple tiles
            [Obj.NODES, ~, NodesGroups] = uniquetol(Obj.NODES, 'ByRows', true); % find multiple occurancies
            NodeInTiles = arrayfun(@(x) NodeInTiles(NodesGroups == x,:), ...
            	1:max(NodesGroups), 'UniformOutput', false);                % group the same tiles
            
            % preallocation
            BarsToAdd = cell(Height*Width,1);
            GroupsToAdd = cell(Height*Width,1);
            
            % loop through the whole structural design domain
            for j=1:Height
                for i=1:Width
                    TileNumber = Obj.TILESNUMBERS(i,j);                     % find the tile number
                    if TileNumber>0                                         % if there is a tile  
                        % get coordinates of the tile's bars
                        BegCoordinates = Obj.TILESNODES{i,j}...
                            (Obj.TILESDEFINITION(TileNumber).BARS(:,1),:);
                        EndCoordinates = Obj.TILESNODES{i,j}...
                            (Obj.TILESDEFINITION(TileNumber).BARS(:,2),:);
                        % get a vector dividing the bars into groups
                        GroupVector = Obj.TILESDEFINITION(TileNumber).GROUPS;
                        % add bars
                        [BarsToAdd{(j-1)*Width+i}, GroupsToAdd{(j-1)*Width+i}] = ...
                            MTAddGlobalBar(Obj, BegCoordinates, EndCoordinates,...
                            NodeInTiles, GroupVector);
                    end
                end
            end
            
            Obj.BARS = vertcat(BarsToAdd{:});
            Obj.BARSGROUPS = vertcat(GroupsToAdd{:});
            [Obj.BARS, ia] = unique(sort(Obj.BARS,2), 'rows');
            Obj.BARSGROUPS = Obj.BARSGROUPS(ia);
            Obj.NUMBARS = size(Obj.BARS,1);
            Obj.NUMNODES = size(Obj.NODES,1);
        end
        
        function [BarsToAdd, GroupsToAdd] = ...
                MTAddGlobalBar(Obj, BegCoors, EndCoors, NodeInTiles, GroupVector)
            % MTAddGlobalBar function adds bars to the initial MTSTRUCT
            % object based on prescribed connections
            %
            % ---INPUT-----------------------------------------------------
            %       Obj                 MTSTRUCT class object
            %       BegCoors            Coordinates of bars' beginning
            %       EndCoors            Coordinates of bars' end
            %       NodeInTiles         Matrix defining in which tiles the 
            %                           individual nodes are
            %       GroupVector         Tile groups vector
            %
            % ---OUTPUT----------------------------------------------------
            %       BarsToAdd           Added bars to the ground structure
            %       GroupsToAdd         Groups belonging to the bars
            
            [~, CheckMat] = ismembertol([BegCoors; EndCoors], ...           % find which begcoors and endcoors are in obj.NODES
                Obj.NODES, 'ByRows', true);   
            BegNodes = CheckMat(1:size(BegCoors,1),:)';                     % nodes corresponding to begcoors
            EndNodes = CheckMat(size(BegCoors,1)+1:end,:)';                 % nodes corresponding to endcoors            
            NotRemove = (BegNodes>0) & (EndNodes>0);                        % do not remove bars where all nodes are inside ground structure
            BegNodes = BegNodes(NotRemove);                                 % remove inner-outer and outer bars
            EndNodes = EndNodes(NotRemove);                                 % remove inner-outer and outer bars
            GroupVector = GroupVector(NotRemove);                           % remove inner-outer and outer bars groups
            BarsToAdd = false(size(BegNodes,2),1);                          % prealocate vector for added bars
            GroupsToAdd = zeros(size(BegNodes,2),1);                        % prealocate cell for bar groups
            
            for i=1:length(BegNodes)                                        % for all bars
                BegTile = NodeInTiles{BegNodes(i)};                         % get all tiles where is the begnode of the bar
                EndTile = NodeInTiles{EndNodes(i)};                         % get all tiles where is the endnode of the bar
                
                % if both the nodes of the bar are in the same tile
                if ismembertol(BegTile, EndTile, 'ByRows', true)
                    BarsToAdd(i) = true;                                    % it should be added
                    GroupsToAdd(i) = GroupVector(i);                        % find bar group
                    
                % otherwise it is needed to check the other tile
                else
                    FirstTileNum = Obj.TILESNUMBERS(BegTile(1),BegTile(2)); % get the first tile number
                    SecondTileNum = Obj.TILESNUMBERS(EndTile(1),EndTile(2));% get the second tile number
                    
                    % find if the bar is present on both tiles
                    [~, IsBoth] = ismembertol([Obj.NODES(BegNodes(i),:) Obj.NODES(EndNodes(i),:); ...
                        Obj.NODES(EndNodes(i),:) Obj.NODES(BegNodes(i),:)], ...
                        [Obj.TILESNODES{BegTile(1),BegTile(2)}(Obj.TILESDEFINITION(FirstTileNum).BARS(:,1),:), ...
                        Obj.TILESNODES{BegTile(1),BegTile(2)}(Obj.TILESDEFINITION(FirstTileNum).BARS(:,2),:); ...
                        Obj.TILESNODES{EndTile(1),EndTile(2)}(Obj.TILESDEFINITION(SecondTileNum).BARS(:,1),:), ...
                        Obj.TILESNODES{EndTile(1),EndTile(2)}(Obj.TILESDEFINITION(SecondTileNum).BARS(:,2),:)],'ByRows', true);
                                             
                    if length(IsBoth)>=2                                    % if the bar is present on both tiles
                        BarsToAdd(i) = true;                                % it should be added
                        GroupsToAdd(i) = GroupVector(i);                    % find bar group
                    end
                end
            end
            
            BarsToAdd = [BegNodes(BarsToAdd)' EndNodes(BarsToAdd)'];
        end
        
        function MTPlotTruss(obj, mode, numgroup, cutoff)
            
            BX = [obj.NODES(obj.BARS(:,1),1)'; obj.NODES(obj.BARS(:,2),1)'];
            BY = [obj.NODES(obj.BARS(:,1),2)'; obj.NODES(obj.BARS(:,2),2)'];
            hold on;
            
            switch mode
                case 'initial'
                    plot(BX, BY, 'LineWidth', 1, 'Color', 'k');
                    
                case 'final'
                    BX = BX(:,obj.A>cutoff);
                    BY = BY(:,obj.A>cutoff);
                    Area = obj.A(obj.A>cutoff);         % only nonzero areas
                    Area = Area/max(Area);              % normalize A to 1
                    Groups = ceil(numgroup*Area);       % divide bars into groups
                    for i=1:numgroup
                        plot(BX(:,(Groups==i)), BY(:,(Groups==i)), ...
                            'LineWidth',3*sqrt(mean(Area(Groups==i))), ...
                            'Color','k');
                    end
                    
                case 'groups'
                    colormat = colorcube(size(obj.GROUPMATRIX,2));
                    for i=size(obj.GROUPMATRIX,2):-1:1
                        Bars = find(obj.GROUPMATRIX(:,i)==1);
                        plot(BX(:,Bars), BY(:,Bars), 'Color', colormat(i,:), ...
                            'LineWidth',rand(1)*10);
                    end
                    
                otherwise
                    error('MTSTRUCT->MTPlotTruss: Unknown mode parameter.');
            end
            
            hold off;
        end
                
        function MTPlotTiles(Obj, mode)
            [width, height] = size(Obj.TILESNUMBERS);
            figure;
            
            switch mode
                case 'grouped'                    
                    hold on;
                    for i=1:width
                        for j=1:height
                            if Obj.TILESNUMBERS(i,j)>0
                                TileNum = Obj.TILESNUMBERS(i,j);
                                TStack = Obj.TILESDEFINITION(TileNum).STACKSIZE;
                                MTPlotTile(Obj.TILESDEFINITION(TileNum),...
                                    [TStack(1)/2+(i-1)*TStack(1) TStack(2)/2+(j-1)*TStack(2)], [], 'labeled');
                            end
                        end
                    end
                    for i=1:width
                        for j=1:height
                            if Obj.TILESNUMBERS(i,j)>0
                                TileNum = Obj.TILESNUMBERS(i,j);
                                TStack = Obj.TILESDEFINITION(TileNum).STACKSIZE;
                                MTPlotTile(Obj.TILESDEFINITION(TileNum),...
                                    [TStack(1)/2+(i-1)*TStack(1) TStack(2)/2+(j-1)*TStack(2)], [], 'inner');
                            end
                        end
                    end
                    MTPlotSupports(Obj);
                    hold off;
                                        
                case 'basic'
                    hold on;
                    % plot boundary
                    for i=1:width
                        for j=1:height
                            if Obj.TILESNUMBERS(i,j)>0
                                TStack = Obj.TILESDEFINITION(1).STACKSIZE;
                                % right
                                if i==width
                                    plot([i*TStack(1) i*TStack(1)], [(j-1)*TStack(2) j*TStack(2)], 'Color', Obj.TILESDEFINITION(1).EDGESCOLOR, 'LineWidth', 1);
                                elseif (Obj.TILESNUMBERS(i+1,j)>=0)==false
                                    plot([i*TStack(1) i*TStack(1)], [(j-1)*TStack(2) j*TStack(2)], 'Color', Obj.TILESDEFINITION(1).EDGESCOLOR, 'LineWidth', 1);
                                end
                                % left
                                if i==1
                                    plot([(i-1)*TStack(1) (i-1)*TStack(1)], [(j-1)*TStack(2) j*TStack(2)], 'Color', Obj.TILESDEFINITION(1).EDGESCOLOR, 'LineWidth', 1);
                                elseif (Obj.TILESNUMBERS(i-1,j)>=0)==false
                                    plot([(i-1)*TStack(1) (i-1)*TStack(1)], [(j-1)*TStack(2) j*TStack(2)], 'Color', Obj.TILESDEFINITION(1).EDGESCOLOR, 'LineWidth', 1);
                                end
                                % top
                                if j==height
                                    plot([(i-1)*TStack(1) i*TStack(1)], [j*TStack(2) j*TStack(2)], 'Color', Obj.TILESDEFINITION(1).EDGESCOLOR, 'LineWidth', 1);
                                elseif (Obj.TILESNUMBERS(i,j+1)>=0)==false
                                    plot([(i-1)*TStack(1) i*TStack(1)], [j*TStack(2) j*TStack(2)], 'Color', Obj.TILESDEFINITION(1).EDGESCOLOR, 'LineWidth', 1);
                                end
                                % bottom
                                % top
                                if j==1
                                    plot([(i-1)*TStack(1) i*TStack(1)], [(j-1)*TStack(2) (j-1)*TStack(2)], 'Color', Obj.TILESDEFINITION(1).EDGESCOLOR, 'LineWidth', 1);
                                elseif (Obj.TILESNUMBERS(i,j-1)>=0)==false
                                    plot([(i-1)*TStack(1) i*TStack(1)], [(j-1)*TStack(2) (j-1)*TStack(2)], 'Color', Obj.TILESDEFINITION(1).EDGESCOLOR, 'LineWidth', 1);
                                end
                            end
                        end
                    end
                    
%                     patch([0 0 width*Obj.TILESDEFINITION(1).STACKSIZE(1) width*Obj.TILESDEFINITION(1).STACKSIZE(1)],...
%                         [0 height*Obj.TILESDEFINITION(1).STACKSIZE(2) height*Obj.TILESDEFINITION(1).STACKSIZE(2) 0],...
%                         1, 'FaceColor', 'none', 'EdgeColor', Obj.TILESDEFINITION(1).EDGESCOLOR, 'LineWidth', 1);
                    MTPlotTruss(Obj, 'final', 10, Obj.MINAREA);
                    MTPlotSupports(Obj);
                    hold off;
                    
                case 'discretized'
                    hold on;
                    for i=1:width
                        for j=1:height
                            if Obj.TILESNUMBERS(i,j)>0
                                TileNum = Obj.TILESNUMBERS(i,j);
                                TStack = Obj.TILESDEFINITION(TileNum).STACKSIZE;
                                MTPlotTile(Obj.TILESDEFINITION(TileNum),...
                                    [TStack(1)/2+(i-1)*TStack(1) TStack(2)/2+(j-1)*TStack(2)], [], 'empty');
                            end
                        end
                    end
                    MTPlotSupports(Obj);
                    hold off;
                    
                case 'initial'
                    hold on;
                    for i=1:width
                        for j=1:height
                            if Obj.TILESNUMBERS(i,j)>0
                                TileNum = Obj.TILESNUMBERS(i,j);
                                TStack = Obj.TILESDEFINITION(TileNum).STACKSIZE;
                                MTPlotTile(Obj.TILESDEFINITION(TileNum),...
                                    [TStack(1)/2+(i-1)*TStack(1) TStack(2)/2+(j-1)*TStack(2)], ...
                                    repmat([0 0 0],max(Obj.TILESDEFINITION(TileNum).GROUPS),1), 'empty');
                            end
                        end
                    end
                    for i=1:width
                        for j=1:height
                            if Obj.TILESNUMBERS(i,j)>0
                                TileNum = Obj.TILESNUMBERS(i,j);
                                TStack = Obj.TILESDEFINITION(TileNum).STACKSIZE;
                                MTPlotTile(Obj.TILESDEFINITION(TileNum),...
                                    [TStack(1)/2+(i-1)*TStack(1) TStack(2)/2+(j-1)*TStack(2)], [], 'ground');
                            end
                        end
                    end
                    MTPlotExtForces(Obj, true);
                    MTPlotSupports(Obj);
                    
                    DimLineWidth = .5;
                    ArrowX = [0 4.5/15 4.5/25 4.5/15];
                    ArrowY = [0 .1 0 -.1];
                    
                    % Important points in horizontal direction
                    holePosX = false(Obj.WIDTH-1,1);
                    for k=1:Obj.WIDTH-1
                        if any(isnan(Obj.TILESNUMBERS(k,:))~=isnan(Obj.TILESNUMBERS(k+1,:)))
                            holePosX(k) = true;
                        end
                    end
                    K1Xx = unique([min(Obj.NODES(:,1)); max(Obj.NODES(:,1)); ...
                        Obj.NODES([Obj.XSUPNODES; Obj.YSUPNODES; Obj.XFORNODES; Obj.YFORNODES],1); ...
                        find(holePosX)*Obj.TILESDEFINITION(1).STACKSIZE(1)]);
                    % placement of dimension labels in Y direction
                    K0Xy = min(Obj.NODES(:,2))-1.0*Obj.TILESDEFINITION(1).STACKSIZE(1);
                    K1Xy = min(Obj.NODES(:,2))-1.5*Obj.TILESDEFINITION(1).STACKSIZE(1);
                    K2Xy = min(Obj.NODES(:,2))-2*Obj.TILESDEFINITION(1).STACKSIZE(1);
                    
                    % Important points in vertical direction
                    holePosY = false(Obj.HEIGHT-1,1);
                    for k=1:Obj.HEIGHT-1
                        if any(isnan(Obj.TILESNUMBERS(:,k))~=isnan(Obj.TILESNUMBERS(:,k+1)))
                            holePosY(k) = true;
                        end
                    end
                    K1Yy = unique([min(Obj.NODES(:,2)); max(Obj.NODES(:,2)); ...
                        Obj.NODES([Obj.XSUPNODES; Obj.YSUPNODES; Obj.XFORNODES; Obj.YFORNODES],2); ...
                        find(holePosY)*Obj.TILESDEFINITION(1).STACKSIZE(2)]);
                    % placement of dimension labels in X direction
                    K0Yx = max(Obj.NODES(:,1))+0.5*Obj.TILESDEFINITION(1).STACKSIZE(1);
                    K1Yx = max(Obj.NODES(:,1))+1*Obj.TILESDEFINITION(1).STACKSIZE(1);
                    K2Yx = max(Obj.NODES(:,1))+1.5*Obj.TILESDEFINITION(1).STACKSIZE(1);
                    
                    % Plot horizontal dimensions
                    hold on;
                    for i=1:numel(K1Xx)-1
                        plot(K1Xx([i i+1]), [K1Xy K1Xy], 'k', 'LineWidth', DimLineWidth);
                        patch(ArrowX+K1Xx(i), ArrowY+K1Xy, 'k', 'FaceColor', 'k');
                        patch(K1Xx(i+1)-ArrowX, ArrowY+K1Xy, 'k', 'FaceColor', 'k');
                        text(mean(K1Xx([i i+1])), K1Xy, strcat('$',num2str(K1Xx(i+1)-K1Xx(i)),'$'), ...
                            'HorizontalAlignment','center', ...
                            'VerticalAlignment', 'bottom', 'Interpreter', 'latex');
                    end
                    for i=1:numel(K1Xx)
                        plot([K1Xx(i) K1Xx(i)], [K0Xy K1Xy], 'k', 'LineWidth', DimLineWidth);
                    end
                    if numel(K1Xx)>2
                        plot(K1Xx([1 end]), [K2Xy K2Xy], 'k', 'LineWidth', DimLineWidth);
                        patch(ArrowX+K1Xx(1), ArrowY+K2Xy, 'k', 'FaceColor', 'k');
                        patch(K1Xx(end)-ArrowX, ArrowY+K2Xy, 'k', 'FaceColor', 'k');
                        text(mean(K1Xx([1 end])), K2Xy, strcat('$',num2str(K1Xx(end)-K1Xx(1)),'$'), ...
                            'HorizontalAlignment','center', ...
                            'VerticalAlignment', 'bottom', 'Interpreter', 'latex');
                        plot([K1Xx(1) K1Xx(1)], [K0Xy K2Xy], 'k', 'LineWidth', DimLineWidth);
                        plot([K1Xx(end) K1Xx(end)], [K0Xy K2Xy], 'k', 'LineWidth', DimLineWidth);
                    end
                    
                    % Plot vertical dimensions
                    hold on;
                    for i=1:numel(K1Yy)-1
                        plot([K1Yx K1Yx], K1Yy([i i+1]), 'k', 'LineWidth', DimLineWidth);
                        patch(ArrowY+K1Yx, ArrowX+K1Yy(i), 'k', 'FaceColor', 'k');
                        patch(ArrowY+K1Yx, K1Yy(i+1)-ArrowX, 'k', 'FaceColor', 'k');
                        text(K1Yx, mean(K1Yy([i i+1])), strcat('$',num2str(K1Yy(i+1)-K1Yy(i)),'$'), ...
                            'HorizontalAlignment','center', 'Rotation', 90,...
                            'VerticalAlignment', 'bottom', 'Interpreter', 'latex');
                    end
                    for i=1:numel(K1Yy)
                        plot([K0Yx K1Yx], [K1Yy(i) K1Yy(i)], 'k', 'LineWidth', DimLineWidth);
                    end
                    if numel(K1Yy)>2
                        plot([K2Yx K2Yx], K1Yy([1 end]), 'k', 'LineWidth', DimLineWidth);
                        patch(ArrowY+K2Yx, ArrowX+K1Yy(1), 'k', 'FaceColor', 'k');
                        patch(ArrowY+K2Yx, K1Yy(end)-ArrowX, 'k', 'FaceColor', 'k');                        
                        text(K2Yx, mean(K1Yy([1 end])), strcat('$',num2str(K1Yy(end)-K1Yy(1)),'$'), ...
                            'HorizontalAlignment','center', 'Rotation', 90,...
                            'VerticalAlignment', 'bottom', 'Interpreter', 'latex');
                        plot([K0Yx K2Yx], [K1Yy(1) K1Yy(1)], 'k', 'LineWidth', DimLineWidth);
                        plot([K0Yx K2Yx], [K1Yy(end) K1Yy(end)], 'k', 'LineWidth', DimLineWidth);
                    end
                                        
                    hold off;
                    
                case 'tiling'
                    hold on;
                    for i=1:width
                        for j=1:height
                            if Obj.TILESNUMBERS(i,j)>0
                                TileNum = Obj.TILESNUMBERS(i,j);
                                TStack = Obj.TILESDEFINITION(TileNum).STACKSIZE;
                                MTPlotTile(Obj.TILESDEFINITION(TileNum),...
                                    [TStack(1)/2+(i-1)*TStack(1) TStack(2)/2+(j-1)*TStack(2)]*1.1, ...
                                    repmat([0 0 0],max(Obj.TILESDEFINITION(i).GROUPS),1), 'labeled');
                            end
                        end
                    end
                    hold off;
                    
                otherwise
                    error('MTSTRUCT: Unknown mode for MTPlotTiles!');
                
            end
            
            axis tight equal off;
            XLim = xlim();
            YLim = ylim();
            set(gca, 'position', [0 0 1 1], 'units', 'normalized');
            Width = 8.4; % cm
            Multiplicator = (XLim(2)-XLim(1))/Width;
            Height = (YLim(2)-YLim(1))/Multiplicator;
            set(gcf, 'PaperUnits', 'centimeters', 'PaperPositionMode', 'auto',...
                'PaperPosition', [0 0 Width Height], 'PaperSize', [Width, Height], ...
                'Renderer', 'Painters');
            
        end
        
        function MTCreateStaticMatrix(Obj)
            % MTCreateStaticMatrix function creates static matrix
            %
            % ---INPUT-----------------------------------------------------
            %       Obj                 MTSTRUCT class object
            
            dxy = Obj.NODES(Obj.BARS(:,2),:)-Obj.NODES(Obj.BARS(:,1),:);    % [deltax, deltay]
            Obj.BARSLENGTHS = sqrt(sum(dxy.^2, 2));                         % bar lengths [m]
            cxy = dxy./repmat(Obj.BARSLENGTHS,1,2);                         % [cos(x) sin(x)]
            b_lok = reshape([-cxy(:,1) -cxy(:,2) cxy(:,1) cxy(:,2)]', ...
                4*Obj.NUMBARS,1);                                           % local geometry matrix
            B_coor1 = reshape(repmat(1:Obj.NUMBARS,4,1),4*Obj.NUMBARS,1);
            B_coor2 = reshape([2*Obj.BARS(:,1)-1, 2*Obj.BARS(:,1), ...
                               2*Obj.BARS(:,2)-1, 2*Obj.BARS(:,2)]', 4*Obj.NUMBARS,1);
            Obj.STATICMATRIX = sparse(B_coor2, B_coor1, b_lok);             % static matrix
            
            if isempty(Obj.IUX)
                Obj.XSUPNODES = zeros(0,1);
            else
                [~, Obj.XSUPNODES] = ismembertol(Obj.IUX,Obj.NODES,'ByRows',true);
                if any(Obj.XSUPNODES==0)
                    error('Some of the XSUPNODES are not acutal nodes.');
                end
            end
            if isempty(Obj.IUY)
                Obj.YSUPNODES = zeros(0,1);
            else
                [~, Obj.YSUPNODES] = ismembertol(Obj.IUY,Obj.NODES,'ByRows',true);
                if any(Obj.YSUPNODES==0)
                    error('Some of the XSUPNODES are not acutal nodes.');
                end
            end
            Obj.STATICMATRIX([Obj.XSUPNODES*2-1 Obj.YSUPNODES*2],:) = [];   % delete supports            
        end
        
        function MTCreateForceVector(Obj)
            % MTCreateForceVector creates nodal forces vector
            %
            % ---INPUT-----------------------------------------------------
            %       Obj                 MTSTRUCT class object

            if isempty(Obj.IFX)
                Obj.IFX = zeros(0, size(Obj.IFY,2));
            end
            if isempty(Obj.IFY)
                Obj.IFY = zeros(0, size(Obj.IFX,2));
            end
            if (size(Obj.IFY,2) == size(Obj.IFX,2))
                if size(Obj.IFY,2)==2
                    lcValsX = ones(size(Obj.IFX,1),1);
                    lcValsY = ones(size(Obj.IFY,1),1);
                    lcVals = 1;
                elseif size(Obj.IFY,2)==3
                    lcVals = unique([Obj.IFY(:,3); Obj.IFX(:,3)]);
                    lcValsX = changem(Obj.IFX(:,3), 1:numel(lcVals), lcVals);
                    lcValsY = changem(Obj.IFY(:,3), 1:numel(lcVals), lcVals);
                else
                    error('ERROR in MTCreateForceVector(): size(,2) must be either 2 or 3.');
                end
            else
                error('ERROR in MTCreateForceVector(): size(,2) of IFX and IFY does not match.');
            end
            if isempty(Obj.IFX)
                Obj.XFORNODES = zeros(0,1);
            else
                [~, Obj.XFORNODES] = ismembertol(Obj.IFX(:,[1 2]), Obj.NODES,'ByRows',true);
            end
            if isempty(Obj.IFY)
                Obj.YFORNODES = zeros(0,1);
            else
                [~, Obj.YFORNODES] = ismembertol(Obj.IFY(:,[1 2]), Obj.NODES,'ByRows',true);
            end
            Obj.FORCEVECTOR = sparse([Obj.XFORNODES*2-1; Obj.YFORNODES*2], [lcValsX; lcValsY], [Obj.FX; Obj.FY], Obj.NUMNODES*2, numel(lcVals));
            Obj.FORCEVECTOR([Obj.XSUPNODES*2-1 Obj.YSUPNODES*2],:) = [];      % delete supports
            Obj.NUMLC = size(Obj.FORCEVECTOR,2);
        end
        
        function MTCreateGroupMatrix(Obj)
            % MTCreateGroupMatrix function creates group matrix based on
            % bars' connections and list of same areas
            %
            % ---INPUT-----------------------------------------------------
            %       obj             MTSTRUCT class object [obj.BARSGROUPS]
            
            Obj.GROUPMATRIX = sparse(1:Obj.NUMBARS, Obj.BARSGROUPS, true);  % create group matrix
            
            if ~isequal(Obj.BARSGROUPS, (1:Obj.NUMBARS)') ...
                    && (size(Obj.GROUPMATRIX,2)~=Obj.NUMGROUPS)             % if the group matrix has lower dimension than it should have
                Obj.GROUPMATRIX(size(Obj.GROUPMATRIX,1),Obj.NUMGROUPS) = 0; % change dimensions
            end
        end
        
        function MTPlotSupports(obj)
            xl = xlim; yl = ylim;
            relsize = mean([xl(2)-xl(1),yl(2)-yl(1)])/10;
            full_supp = obj.XSUPNODES(ismember(obj.XSUPNODES, obj.YSUPNODES));
            hold on;
            for i=1:length(full_supp)
                x_c = [0 -0.6 0.6]; y_c = [0 -.6 -.6];
                if obj.NODES(full_supp(i),2)==obj.HEIGHT*obj.TILESDEFINITION(1).STACKSIZE(2)
                    y_c = -y_c;
                end
                patch(x_c*relsize+obj.NODES(full_supp(i),1), y_c*relsize+obj.NODES(full_supp(i),2), 1, ...
                    'EdgeColor', 'k', 'LineWidth', 0.5, 'FaceColor', 'none');
            end
            hold off;
        end
        
        function MTPlotExtForces(obj, useLabel)
            if ~exist('useLabel','var')
                useLabel = false;
            end
            
            hold on;
            MaxLoad = max(abs([obj.FX(:); obj.FY(:)]));
            xl = xlim; yl = ylim;
            relsize = mean([xl(2)-xl(1),yl(2)-yl(1)])/10;
            cMap = prism(obj.NUMLC);
            
            % X-dir
            for j=1:size(obj.FX,1)
                if size(obj.IFX,2)>2
                    lc = obj.IFX(j,3);
                else
                    lc = 1;
                end
                stycnik = obj.XFORNODES(j);
                delka_sily = max(abs(obj.FX(j)/MaxLoad*relsize*3), relsize*5/4)*sign(obj.FX(j))/1.5;
                delka_sily2 = relsize*3/1.7;
                Xcoor = [obj.NODES(stycnik,1), obj.NODES(stycnik,1)-delka_sily2/3*sign(obj.FX(j)), ...
                    obj.NODES(stycnik,1)-delka_sily2/3*sign(obj.FX(j)), obj.NODES(stycnik,1)-delka_sily, ...
                    obj.NODES(stycnik,1)-delka_sily, obj.NODES(stycnik,1)-delka_sily2/3*sign(obj.FX(j)), ...
                    obj.NODES(stycnik,1)-delka_sily2/3*sign(obj.FX(j))];
                Ycoor = [obj.NODES(stycnik,2), obj.NODES(stycnik,2)-delka_sily2/6, ...
                    obj.NODES(stycnik,2)-delka_sily2/12, obj.NODES(stycnik,2)-delka_sily2/12, ...
                    obj.NODES(stycnik,2)+delka_sily2/12, obj.NODES(stycnik,2)+delka_sily2/12, ...
                    obj.NODES(stycnik,2)+delka_sily2/6];
                patch(Xcoor, Ycoor, cMap(lc,:), 'EdgeColor', 'k', 'LineWidth', .5);
                if useLabel
                    text(mean([obj.NODES(stycnik,1), obj.NODES(stycnik,1)-delka_sily]), obj.NODES(stycnik,2)+0.2, strcat('$',num2str(abs(obj.FX(j))),'$'), ...
                        'Interpreter', 'latex', 'HorizontalAlignment','center');
                end
            end

            % Y-dir
            for j=1:size(obj.FY,1)
                if size(obj.IFY,2)>2
                    lc = obj.IFY(j,3);
                else
                    lc = 1;
                end
                stycnik = obj.YFORNODES(j);
                delka_sily = max(abs(obj.FY(j)/MaxLoad*relsize*3), relsize*5/4)*sign(obj.FY(j))/1.5;
                delka_sily2 = relsize*3/1.7;
                Xcoor = [obj.NODES(stycnik,1), obj.NODES(stycnik,1)-delka_sily2/6, ...
                    obj.NODES(stycnik,1)-delka_sily2/12, obj.NODES(stycnik,1)-delka_sily2/12, ...
                    obj.NODES(stycnik,1)+delka_sily2/12, obj.NODES(stycnik,1)+delka_sily2/12, ...
                    obj.NODES(stycnik,1)+delka_sily2/6];
                Ycoor = [obj.NODES(stycnik,2), obj.NODES(stycnik,2)-delka_sily2/3*sign(obj.FY(j)), ...
                    obj.NODES(stycnik,2)-delka_sily2/3*sign(obj.FY(j)), obj.NODES(stycnik,2)-delka_sily, ...
                    obj.NODES(stycnik,2)-delka_sily, obj.NODES(stycnik,2)-delka_sily2/3*sign(obj.FY(j)), ...
                    obj.NODES(stycnik,2)-delka_sily2/3*sign(obj.FY(j))];
                patch(Xcoor, Ycoor, cMap(lc,:), 'EdgeColor', 'k', 'LineWidth', .5);
                if useLabel
                    text(obj.NODES(stycnik,1)+0.2, mean([obj.NODES(stycnik,2), obj.NODES(stycnik,2)-delka_sily]), strcat('$',num2str(abs(obj.FY(j))),'$'), ...
                        'Interpreter', 'latex', 'HorizontalAlignment','left');
                end
            end
            hold off;
        end
        
        function MTOptimizeGroundStructure(Obj, ObjType, Options)
            % MTOptimizeGroundStructure function
            %
            % ---INPUT-----------------------------------------------------
            %       Obj                 MTSTRUCT class object
                        
            if isfield(Options, 'Display') && strcmp(Options.Display, 'on')
                fprintf('MTSRTUCT: Starting optimization...\n');
                YalmipSet = sdpsettings('verbose',1,'cachesolvers',1,'solver','mosek,*');
            else
                YalmipSet = sdpsettings('verbose',0,'cachesolvers',1,'solver','mosek,*');
            end
            
            % create group matrix based on optimization type
            switch ObjType
                case 'lp_basic'
                    Obj.BARSGROUPS = (1:Obj.NUMBARS)';
                    formulation = 'lp';
                    
                case 'lp_groups'
                    formulation = 'lp';
                    error('ERROR: Lp does not allow for modularity.');
                    
                case 'socp_basic'
                    Obj.BARSGROUPS = (1:Obj.NUMBARS)';
                    formulation = 'socp';
                    
                case 'socp_groups'
                    formulation = 'socp';
                    
                case 'sdp_basic'
                    Obj.BARSGROUPS = (1:Obj.NUMBARS)';
                    formulation = 'sdp';
                    
                case 'sdp_groups'
                    formulation = 'sdp';
                    
                otherwise
                    error('MTSTRUCT: Unknown optimization type!');
            end
            
            % Create group matrix
            MTCreateGroupMatrix(Obj);
            
            % linear programming - plastic formulation - minimize volume
            switch formulation
                case 'lp'
                    % TODO: It might be good to compute compliance
                    % (lower-bound to elastic design)
                    if Obj.NUMLC > 1
                        error('LP does not allow for multiple load cases');
                    end
                    
                    Sigma = sqrt(Obj.E);                                    % this corresponds to the dual (see Bendsoe)
                    
                    % define design variables
                    a = sdpvar(size(Obj.GROUPMATRIX,2),1);                  % cross-sections
                    sC = sdpvar(Obj.NUMBARS,1);                             % compressional forces
                    sT = sdpvar(Obj.NUMBARS,1);                             % tensional forces
                    
                    % [A -A]*[sT; sC] = F
                    Equilibrium = [Obj.STATICMATRIX -Obj.STATICMATRIX]* [sT; sC] == Obj.FORCEVECTOR;
                    % [-G 1/sigma 1/sigma]*[a; sT; sC] <= 0
                    StressRel = [-Obj.GROUPMATRIX, speye(Obj.NUMBARS)/Sigma ...
                          speye(Obj.NUMBARS)/Sigma] * [a; sT; sC] <= 0;
                    NonNeg = [a; sT; sC] >= 0;
                    
                    % Lengths - vector of bar lengths (here we cummulate them based on groups)
                    if size(Obj.GROUPMATRIX, 1) == size(Obj.GROUPMATRIX, 2)
                        Lengths = Obj.BARSLENGTHS;
                    else
                        Lengths = zeros(size(Obj.GROUPMATRIX, 2),1);
                        for i=1:length(Obj.TILESDEFINITION)
                            TempLength = full(sparse(Obj.TILESDEFINITION(i).GROUPS, ...
                                1, Obj.TILESDEFINITION(i).LENGTHS'));
                            Lengths(1:length(TempLength)) = Lengths(1:length(TempLength)) + ...
                                TempLength*nnz(Obj.TILESNUMBERS==i);
                        end
                    end
                    
                    Diagnostics = solvesdp([Equilibrium; StressRel; NonNeg], Lengths'*a, YalmipSet);
                    
                    if Diagnostics.problem == 1
                        fprintf('MTSTRUCT: The problem %d is probably infeasible!\n',...
                            bi2de(Obj.CONNECTIVITYMATRIX(:)'));
                    elseif (Diagnostics.problem ~= 0) && (Diagnostics.problem ~= 1)
                        fprintf('MTSTRUCT: The problem %d has unknown status!\n',...
                            bi2de(Obj.CONNECTIVITYMATRIX(:)'));
                    end
                    
                    Obj.OBJECTIVE = value(Lengths' * double(a));
                    Obj.A = double(a);
                    Obj.A(Obj.A<=Obj.MINAREA+eps) = 0;
                    
                    if size(Obj.GROUPMATRIX, 1) ~= size(Obj.GROUPMATRIX, 2)
                        for i=1:length(Obj.TILESDEFINITION)
                            Obj.TILESDEFINITION(i).AREAS = Obj.A(Obj.TILESDEFINITION(i).GROUPS);
                            Obj.TILESDEFINITION(i).MAXAREA = max(Obj.A);
                        end
                    end
                    
                    Obj.A = Obj.GROUPMATRIX*Obj.A;                          % get cross-sectional areas for bars (not groups)

                    if isfield(Options, 'Display') && strcmp(Options.Display, 'on')
                        fprintf('MTSTRUCT: Final objective is %f.\n', Obj.OBJECTIVE);
                    end
                    
                case 'socp'
                    % Lengths - vector of bar lengths (here we cummulate them based on groups)
                    if size(Obj.GROUPMATRIX, 1) == size(Obj.GROUPMATRIX, 2)
                        Lengths = Obj.BARSLENGTHS;
                    else
                        Lengths = zeros(size(Obj.GROUPMATRIX, 2),1);
                        for i=1:length(Obj.TILESDEFINITION)
                            TempLength = full(sparse(Obj.TILESDEFINITION(i).GROUPS, ...
                                1, Obj.TILESDEFINITION(i).LENGTHS'));
                            Lengths(1:length(TempLength)) = Lengths(1:length(TempLength)) + ...
                                TempLength*nnz(Obj.TILESNUMBERS==i);
                        end
                    end

%                     % define design variables
%                     aak = sdpvar(size(Obj.GROUPMATRIX,2),1);                 % cross-sections
%                     ak = aak*1e-3;                                           % scaling (better stability of solution)
%                     ssk = sdpvar(Obj.NUMBARS, Obj.NUMLC, 'full');             % internal forces
%                     sk = ssk*1e-3;
%                     tauk = sdpvar(Obj.NUMBARS,1);                            % complementary strain energy
%                     
%                     ConeXK = [tauk-Obj.GROUPMATRIX*ak, repmat(sqrt(2*Obj.BARSLENGTHS/Obj.E),1,Obj.NUMLC).*sk];
%                     ConeYK = tauk + ak(Obj.BARSGROUPS);
%                     ConstraintsK = [aak >= 0; ...
%                                     Obj.STATICMATRIX * sk  == Obj.FORCEVECTOR; ...
%                                     Lengths'*ak <= Obj.VOLBOUND; ...
%                                     cone([ConeYK(:)'; ConeXK'])];
%                     if ~isinf(Obj.STRESSBOUNDTENSION)
%                         if ~isinf(Obj.STRESSBOUNDCOMPRESSION)
%                             ConstraintsK = [ConstraintsK; repmat(Obj.STRESSBOUNDCOMPRESSION*Obj.GROUPMATRIX*ak,1,Obj.NUMLC)<=sk<=repmat(Obj.STRESSBOUNDTENSION*Obj.GROUPMATRIX*ak,1,Obj.NUMLC)];
%                         end
%                         % add only tension bound
%                         ConstraintsK = [ConstraintsK; sk<=repmat(Obj.STRESSBOUNDTENSION*Obj.GROUPMATRIX*ak,1,Obj.NUMLC)];
%                     else
%                         if ~isinf(Obj.STRESSBOUNDCOMPRESSION)
%                             % add compression bound
%                             ConstraintsK = [ConstraintsK; sk>=repmat(Obj.STRESSBOUNDCOMPRESSION*Obj.GROUPMATRIX*ak,1,Obj.NUMLC)];
%                         end
%                     end
%                     
%                     Diagnostics = solvesdp(ConstraintsK, sum(tauk), YalmipSet);
                   
                    % aggregated version, perform scaling!
                    % V = l*a -> a = V/l
%                     expectedCS = Obj.VOLBOUND/sum(Obj.BARSLENGTHS)/15;      % assuming 1/10 of the bars vanishes
                    expectedCS = 1e-3;
                    aakg = sdpvar(size(Obj.GROUPMATRIX,2),1);               % cross-sections
                    akg = aakg*expectedCS;                                  % scaling (better stability of solution)
                    % internal forces are expected in the magnitude of
                    % loads
%                     expectedS = max(max(abs(Obj.FORCEVECTOR)))/Obj.NUMBARS;
                    expectedS = 1e-3;
                    sskg = sdpvar(Obj.NUMBARS, Obj.NUMLC, 'full');          % internal forces
                    skg = sskg*expectedS;                                   % scaling (better stability of solution)
                    % 
                    tautaukg = sdpvar(size(Obj.GROUPMATRIX,2),1);           % complementary strain energy
                    taukg = tautaukg;
                    cf = repmat(sqrt(2*Obj.BARSLENGTHS/Obj.E),1,Obj.NUMLC).*skg;
                    
                    % cones
                    maxNumBarsInGroup = max(sum(Obj.GROUPMATRIX,1));
                    presentGroups = any(Obj.GROUPMATRIX,1);
                    CMRow = zeros(size(cf));
                    CMCol = zeros(size(cf));
                    newCols = double(full(presentGroups));
                    newCols(presentGroups) = 1:sum(presentGroups);
                    for gr=1:size(Obj.GROUPMATRIX,2)
                        grBars = Obj.GROUPMATRIX(:,gr);
                        if any(grBars)
                            CMRow(grBars,:)= reshape(1:sum(grBars)*Obj.NUMLC, sum(grBars), Obj.NUMLC);
                            CMCol(grBars,:) = ones(sum(grBars), Obj.NUMLC)*newCols(gr);
                        end
                    end
                    ConeKG = cone([(taukg(presentGroups) + akg(presentGroups))'; ...
                                    (taukg(presentGroups)-akg(presentGroups))'; ...
                                    sparse(CMRow(:), CMCol(:), cf(:), maxNumBarsInGroup*Obj.NUMLC, sum(presentGroups))]);
                    ConstraintsKG = [aakg >= 0; ...
                                    Obj.STATICMATRIX * skg == Obj.FORCEVECTOR; ...
                                    Lengths'*akg <= Obj.VOLBOUND; ...
                                    ConeKG];
                    if ~isinf(Obj.STRESSBOUNDTENSION)
                        if ~isinf(Obj.STRESSBOUNDCOMPRESSION)
                            ConstraintsKG = [ConstraintsKG; repmat(Obj.STRESSBOUNDCOMPRESSION*Obj.GROUPMATRIX*akg,1,Obj.NUMLC)<=skg<=repmat(Obj.STRESSBOUNDTENSION*Obj.GROUPMATRIX*akg,1,Obj.NUMLC)];
                        end
                        % add only tension bound
                        ConstraintsKG = [ConstraintsKG; skg<=repmat(Obj.STRESSBOUNDTENSION*Obj.GROUPMATRIX*akg,1,Obj.NUMLC)];
                    else
                        if ~isinf(Obj.STRESSBOUNDCOMPRESSION)
                            % add compression bound
                            ConstraintsKG = [ConstraintsKG; skg>=repmat(Obj.STRESSBOUNDCOMPRESSION*Obj.GROUPMATRIX*akg,1,Obj.NUMLC)];
                        end
                    end
                    
                    DiagnosticsK = solvesdp(ConstraintsKG, (sum(Obj.GROUPMATRIX,1)>0)*taukg, YalmipSet);
                    
                    if DiagnosticsK.problem == 1
                        fprintf('MTSTRUCT: The problem is probably infeasible!\n');
                    elseif DiagnosticsK.problem == 2
                        fprintf('MTSTRUCT: Unbounded objective function.\n');
                    elseif (DiagnosticsK.problem ~= 0) && (DiagnosticsK.problem ~= 1)
                        fprintf('MTSTRUCT: The problem has unknown status!\n');
                    end
                    
%                     stresses = double(sk)./(Obj.GROUPMATRIX*double(ak));
%                     fprintf('Maximum stress: %f\n', max(stresses));
%                     fprintf('Minimum stress: %f\n', min(stresses));
%                     fprintf('Objective: %f\n', sum(double(tauk)));
%                     fprintf('Time: %f\n', Diagnostics.solvertime);
%                     
%                     stressesg = double(skg)./(Obj.GROUPMATRIX*double(akg));
%                     fprintf('Maximum stress: %f\n', max(stressesg));
%                     fprintf('Minimum stress: %f\n', min(stressesg));
%                     fprintf('Objective: %f\n', (sum(Obj.GROUPMATRIX,1)>0)*double(taukg));
%                     fprintf('Time: %f\n', DiagnosticsK.solvertime);
                    
%                     % plot f1
%                     Obj.A = double(ak);
%                     Obj.A(Obj.A<=Obj.MINAREA+eps) = 0;
%                     Obj.A = Obj.GROUPMATRIX*Obj.A;
%                     MTPlotTiles(Obj, 'grouped');
%                     print('L/T1.png','-dpng');
%                     close all;
%                     
%                     % plot f2
%                     Obj.A = double(akg);
%                     Obj.A(Obj.A<=Obj.MINAREA+eps) = 0;
%                     Obj.A = Obj.GROUPMATRIX*Obj.A;
%                     MTPlotTiles(Obj, 'grouped');
%                     print('L/T2.png','-dpng');
%                     close all;
                    
                    Obj.OBJECTIVE = (sum(Obj.GROUPMATRIX,1)>0)*double(taukg);
                    Obj.A = double(akg);
                    Obj.A(Obj.A<=Obj.MINAREA+eps) = 0;

                    if size(Obj.GROUPMATRIX, 1) ~= size(Obj.GROUPMATRIX, 2)
                        for i=1:length(Obj.TILESDEFINITION)
                            Obj.TILESDEFINITION(i).AREAS = Obj.A(Obj.TILESDEFINITION(i).GROUPS);
                            Obj.TILESDEFINITION(i).MAXAREA = max(Obj.A);
                        end
                    end
                    
                    Obj.A = Obj.GROUPMATRIX*Obj.A;                          % get cross-sectional areas for bars (not groups)

                    if isfield(Options, 'Display') && strcmp(Options.Display, 'on')
                        fprintf('MTSTRUCT: Final objective is %f.\n', Obj.OBJECTIVE);
                    end
                    
                case 'sdp'
                    
                    % define variables
                    a = sdpvar(size(Obj.GROUPMATRIX,2),1);                  % cross-sections
                    gamma = sdpvar(Obj.NUMLC,1);                            % doubled compliance
                  
                    % LMI
                    % stiffness matrix
                    Kel = cell(size(Obj.GROUPMATRIX,2),1);
                    for i=1:size(Obj.GROUPMATRIX,2)
                        BarsInGroup = find(Obj.GROUPMATRIX(:,i))';
                        Kgl = cell(numel(BarsInGroup),1);
                        iter = 1;
                        for j=BarsInGroup
                            Kbj = Obj.E/Obj.BARSLENGTHS(j)*[Obj.STATICMATRIX(:,j); 0]*[Obj.STATICMATRIX(:,j)', 0];
                            [rr,cc,vv] = find(Kbj);
                            Kgl{iter} = [rr,cc,vv*a(i)];
                            iter = iter+1;
                        end
                        Kel{i} = vertcat(Kgl{:});
                    end
                    
                    Kall = vertcat(Kel{:});
                    LMI0 = sparse(Kall(:,1), Kall(:,2), Kall(:,3), size(Obj.FORCEVECTOR,1)+1, size(Obj.FORCEVECTOR,1)+1);
                    LMI = cell(Obj.NUMLC,1);
                    for i=1:Obj.NUMLC
                        Fi = sparse([1:size(Obj.FORCEVECTOR,1), (size(Obj.FORCEVECTOR,1)+1)*ones(1,size(Obj.FORCEVECTOR,1))], ...
                                    [(size(Obj.FORCEVECTOR,1)+1)*ones(1,size(Obj.FORCEVECTOR,1)), 1:size(Obj.FORCEVECTOR,1)], ...
                                    [Obj.FORCEVECTOR(:,i) Obj.FORCEVECTOR(:,i)]);
                        Fg = sparse(size(Obj.FORCEVECTOR,1)+1, size(Obj.FORCEVECTOR,1)+1, gamma(i));
                        LMI{i} = (LMI0 + Fi + Fg)>=0;
                    end
                    
                    % Lengths - vector of bar lengths (here we cummulate them based on groups)
                    if size(Obj.GROUPMATRIX, 1) == size(Obj.GROUPMATRIX, 2)
                        Lengths = Obj.BARSLENGTHS;
                    else
                        Lengths = zeros(size(Obj.GROUPMATRIX, 2),1);
                        for i=1:length(Obj.TILESDEFINITION)
                            TempLength = full(sparse(Obj.TILESDEFINITION(i).GROUPS, ...
                                1, Obj.TILESDEFINITION(i).LENGTHS'));
                            Lengths(1:length(TempLength)) = Lengths(1:length(TempLength)) + ...
                                TempLength*nnz(Obj.TILESNUMBERS==i);
                        end
                    end
                    
                    Diagnostics = solvesdp([LMI{:}; a>=0; Lengths'*a <= Obj.VOLBOUND], sum(gamma), YalmipSet);
                    
                    if Diagnostics.problem == 1
                        fprintf('MTSTRUCT: The problem %d is probably infeasible!\n',...
                            bi2de(Obj.CONNECTIVITYMATRIX(:)'));
                    elseif (Diagnostics.problem ~= 0) && (Diagnostics.problem ~= 1)
                        fprintf('MTSTRUCT: The problem %d has unknown status!\n',...
                            bi2de(Obj.CONNECTIVITYMATRIX(:)'));
                    end
                    
                    Obj.OBJECTIVE = 1/2 * double(sum(gamma));               % this is needed to check
                    Obj.A = double(a);                                      % extract areas
                    Obj.A(Obj.A<=Obj.MINAREA+eps) = 0;
                    
                    if size(Obj.GROUPMATRIX, 1) ~= size(Obj.GROUPMATRIX, 2)
                        for i=1:length(Obj.TILESDEFINITION)
                            Obj.TILESDEFINITION(i).AREAS = Obj.A(Obj.TILESDEFINITION(i).GROUPS);
                            Obj.TILESDEFINITION(i).MAXAREA = max(Obj.A);
                        end
                    end
                    
                    Obj.A = Obj.GROUPMATRIX*Obj.A;                          % get cross-sectional areas for bars (not groups)

                    if isfield(Options, 'Display') && strcmp(Options.Display, 'on')
                        fprintf('MTSTRUCT: Final objective is %f.\n', Obj.OBJECTIVE);
                    end
                    
                otherwise
                    error('MTSTRUCT: Unknown optimization formulation.');
            end            
        end
            
        function MTProduceTilesNumbers(Obj, Symmetry)
            % MTProduceTilesNumbers function produces real tile numbers
            %   based on connectivity matrix and possible symmetry
            %
            % ---INPUT-----------------------------------------------------
            %       Obj                 MTSTRUCT class object
            %       Symmetry            true if forced symmetry of Conn. m.
                        
            Width = Obj.WIDTH; Height = Obj.HEIGHT;
            
            if Symmetry
                if mod(Width, 2)
                    C = [Obj.CONNECTIVITYMATRIX, fliplr(Obj.CONNECTIVITYMATRIX)]; % structural connectivity matrix for full structure
                else
                    C = [Obj.CONNECTIVITYMATRIX, fliplr(Obj.CONNECTIVITYMATRIX(:,1:end-1))];
                end
            else
%                 Width = size(Obj.CONNECTIVITYMATRIX,2)-1;                   % structural width
%                 Height = size(Obj.CONNECTIVITYMATRIX,1)-1;                  % structural height
                C = Obj.CONNECTIVITYMATRIX;                                 % structural connectivity matrix
            end
            
            Obj.TILESNUMBERS = zeros(Width, Height);                        % preallocation
            for i=1:Width
                for j=1:Height
                    Vertices = reshape(C([Height-j+1 Height-j+2],[i i+1]),4,1);
                    if any(isnan(Vertices))
                        Obj.TILESNUMBERS(i,j) = nan;
                    else
                        Obj.TILESNUMBERS(i,j) = [1 2 4 8]*Vertices + 1;     % compute the real number
                    end
                end
            end
        end
        
        function MTSetTiles(ConnectivityMatrix, Obj, Symmetry)
            % MTSetTiles function ensures proper interaction between tiles
            %
            % ---INPUT-----------------------------------------------------
            %       ConnectivityMatrix  binary connectivity matrix
            %       Obj                 MTSTRUCT class object
            %       Symmetry            true if forced symmetry of Conn. m.

            Obj.CONNECTIVITYMATRIX = ConnectivityMatrix;                    % assign connectivity matrix
            MTProduceTilesNumbers(Obj, Symmetry);                           % get real tiles numbers
            MTCreateGroundStructure(Obj);                                   % create ground structure
            MTCreateStaticMatrix(Obj);                                      % create static matrix
            MTCreateForceVector(Obj);                                       % create force vector
        end
        
        function ObjVal = MTOptimizeBinary(ConnectivityMatrix, Obj, Symmetry, OptType, Options)
            % MTOptimizeBinary function finds globally optimal topology for
            %   a given connectivity matrix
            %
            % ---INPUT-----------------------------------------------------
            %       ConnectivityMatrix  binary connectivity matrix
            %       Obj                 MTSTRUCT class object
            %       Symmetry            true if forced symmetry of Conn. m.
            %       OptType             formulation of optimization
            %       Options             options passed to optimization
            %
            % ---OUTPUT----------------------------------------------------
            %       ObjVal              optimal objective value
            
            if ~exist('Options','var')
                Options = [];
            end
            
            if ~isempty(Obj.PRESCRIBEDVERTICES)
                ConnectivityMatrix(Obj.PRESCRIBEDVERTICES) = Obj.PRESCRIBEDVERTICESVALUES(Obj.PRESCRIBEDVERTICES);
            end
            
            MTSetTiles(ConnectivityMatrix, Obj, Symmetry);                  % ensure proper tiles interactions
            MTOptimizeGroundStructure(Obj, OptType, Options);               % find optimal topology for ground structure
            ObjVal = Obj.OBJECTIVE;                                         % extract objective value
            
%             MTPlotTiles(Obj, 'grouped');
%             cMat = ConnectivityMatrix(:);
%             cMat(isnan(cMat)) = [];
%             print(['L/', num2str(ObjVal),'_', bin2text(cMat),'.png'], '-dpng');
%             close all;
%             
%             MTPlotTileSet(Obj.TILESDEFINITION, 'full');
%             print(['sa_DTU/',num2str(Num, '%04.f'),'_set.png'], '-dpng');
%             close all;
        end
            
        function MTAddTiles(Obj, Tiles)
            % MTAddTiles function adds tiles to MTSTRUCT object
            %
            % ---INPUT-----------------------------------------------------
            %       Obj                 MTSTRUCT class object
            %       Tiles            	vector of MTTILE class objects
            
            Obj.TILESDEFINITION = [Obj.TILESDEFINITION, Tiles];             % add tiles
            Obj.NUMGROUPS = max(arrayfun(@(x) max(Tiles(x).GROUPS), ...
                1:length(Tiles)));                                          % find maximum group number
        end
        
        function New = MTCopy(Obj)
            % MTCopy function copies the handle class
            %
            % ---INPUT-----------------------------------------------------
            %       Obj             MTSTRUCT class object
            %
            % ---OUTPUT----------------------------------------------------
            %       New             MTSTRUCT class object (vector)
            
            % Instantiate new object of the same class.
            New = feval(class(Obj));
            p = properties(Obj);                                            % copy all nonhidden properties
            for i = 1:length(p)
                if strcmp(p{i}, 'TILESDEFINITION')
                    New.(p{i}) = MTTILE;
                    for j = length(Obj.(p{i})):-1:1
                        New.(p{i})(j) = MTCopy(Obj.TILESDEFINITION(j), 1);
                    end
                else
                    New.(p{i}) = Obj.(p{i});
                end
            end    
        end
        
        function [BeamOpt, BeamIdeal, BeamWorst, Misc] = MTOptimizeTwoLevel(Obj, TopoOptFormulation, AssemblyOptFormulation, SymmetricTiling)
            % MTOptimizeTwoLevel function performs two-level optimization
            %
            % ---INPUT-----------------------------------------------------
            %       Obj                 MTSTRUCT class object
            %       TopoOptFormulation      'sdp', 'socp' or 'lp'
            %       AssemblyOptFormulation  'ga' or 'sa'
            %       SymmetricTiling         true/false
            %
            % ---OUTPUT----------------------------------------------------
            %       BeamOpt                 design reached by two-level opt
            %       BeamIdeal               ideal design
            %       BeamWorst               worst-case design
            
            % Define setting for the optimization type
            switch TopoOptFormulation
                case 'lp'
                    TopoFormIdeal = 'lp_basic';
                    TopoFormModular = 'lp_groups';
                    Obj.MINAREA = 1e-6;
                case 'socp'
                    TopoFormIdeal = 'socp_basic';
                    TopoFormModular = 'socp_groups';
                    Obj.MINAREA = 1e-4;
                case 'sdp'
                    TopoFormIdeal = 'sdp_basic';
                    TopoFormModular = 'sdp_groups';
                    Obj.MINAREA = 5e-3;
                otherwise
                    error('MTSTRUCT: Unknown optimization formulation!');
            end
            
            % Detect available solvers in YALMIP; this needs to be done
            % only once
            try
                optimize([]);
            catch
                error('MTSTRUCT: YALMIP has not been detected!');
            end
            
            % Define the worst-case tiling connectivity matrix
            if SymmetricTiling
                CWorst = zeros(Obj.HEIGHT+1, round((Obj.WIDTH+1)/2));
            else
                CWorst = zeros(Obj.HEIGHT+1, Obj.WIDTH+1);
            end
            ObjWorst = MTOptimizeBinary(CWorst, Obj, SymmetricTiling,...
                TopoFormModular);                                           % obtain the worst-case result
            BeamWorst = MTCopy(Obj);                                        % copy the result
            fprintf('Worst-case objective: %f\n',ObjWorst);

%             MTSetTiles(CWorst, Obj, SymmetricTiling);                       % ensure proper tiles interactions
%             if ~isempty(Obj.PRESCRIBEDVERTICES)
%                 ConnectivityMatrix(Obj.PRESCRIBEDVERTICES) = Obj.PRESCRIBEDVERTICESVALUES(Obj.PRESCRIBEDVERTICES);
%             end
            MTOptimizeGroundStructure(Obj, TopoFormIdeal, 'on');            % obtain the ideal non-tiled solution
            BeamIdeal = MTCopy(Obj);
            ObjIdeal = BeamIdeal.OBJECTIVE;                                 % copy the best objective
            fprintf('Ideal non-tiled beam objective: %f\n',ObjIdeal);

            switch AssemblyOptFormulation
                case 'sa'                                                   % simulated annealing
                    % Parameters set for SA
                    NumTemp = round(size(CWorst,1)*size(CWorst,2)*32);       % number of system coolings
                    NumSteps = 1;                                           % number of iterations for the same temperature
                    T0 = (ObjWorst - ObjIdeal)/25;                          % compute the initial temperature

                    % Functions
                    CoolingFcn = @(T) exp((640/NumTemp)*log(0.99))*T;
                    NeighborFcn = @(Omega,Iter,IMax)...
                        MTSTRUCT.MTSASelectNeighbor(Omega, Iter, IMax);

                    % Generate random connectivity matrix
                    if SymmetricTiling
                        C = randi([0 1], Obj.HEIGHT+1, round((Obj.WIDTH+1)/2));
                    else
                        C = randi([0 1], Obj.HEIGHT+1, Obj.WIDTH+1);
                    end
                    
                    % Break solution symmetry -- this element is not
                    % permited to change in neighbor function
                    % TODO: lexicographic constraint should be somehow
                    % added
%                     SAOpt.FixVals = [round(size(CWorst,2)/2), round(size(CWorst,1)/2), 0];                                % [x y value]
                    SAOpt.FixVals = [];
                    
%                     % For the enumerated case avoid duplicate
%                     % computations...
%                     run './ENUMERATION_8x3/loadenum.m';
%                     Compliances = [ObjVals; flipud(ObjVals)];
%                     [COpt, ~, Misc] = MTSimulatedAnnealing(@(x) ...
%                         Compliances(bi2de(x(:)')+1), ...
%                         C, T0, NumTemp, NumSteps, CoolingFcn, NeighborFcn, SAOpt);
                    
                    % Start the two-phase optimization using SA and topology optimization
                    [COpt, ~, Misc] = MTSimulatedAnnealing(@(x) ...
                        MTOptimizeBinary(x, Obj, SymmetricTiling, TopoFormModular), ...
                        C, T0, NumTemp, NumSteps, CoolingFcn, NeighborFcn, SAOpt);
                    
                case 'ga'                                                   % genetic algorithm
                    Chromosome.X = size(CWorst,2);
                    Chromosome.Y = size(CWorst,1);

                    % Settings
%                     GAOpt.FixVals = [round(size(CWorst,2)/2), round(size(CWorst,1)/2), 0];                                     % [x y value]
                    [fixPosX,fixPosY] = find(Obj.PRESCRIBEDVERTICES);
                    fixPosVal = Obj.PRESCRIBEDVERTICESVALUES(Obj.PRESCRIBEDVERTICES);
                    GAOpt.FixVals = [fixPosX(:), fixPosY(:), fixPosVal(:)];
                    GAOpt.CrossoverProbability = 0.94;
                    GAOpt.MutationProbability = 1/(numel(CWorst)-size(GAOpt.FixVals,1));
                    GAOpt.PopulationSize = round(3.6*sqrt(numel(CWorst)-size(GAOpt.FixVals,1)));
                    GAOpt.GenerationsCount = round(2.45*GAOpt.PopulationSize/5)*5;
                    GAOpt.Elitism = true;
                    GAOpt.CrossoverType = 'uniform';
                    GAOpt.SelectionType = 'tournament-selection';
                    GAOpt.Tournament.Size = round(sqrt(numel(CWorst)-size(GAOpt.FixVals,1))*4/3);
                    GAOpt.Tournament.Probability = 0.30;
                    GAOpt.Diversity = false;
                    GAOpt.UseParallel = false;

                    if GAOpt.UseParallel
                        NumWorkers = min([GAOpt.PopulationSize, ...
                            java.lang.Runtime.getRuntime().availableProcessors]);       % number of logical cores
                    else
                        NumWorkers = 1;
                    end

%                     % For the enumerated case avoid duplicate
%                     % computations...ss
%                     run './ENUMERATION_8x3/loadenum.m';
%                     Compliances = [ObjVals; flipud(ObjVals)];
                    
                    if NumWorkers>1                                                     % if parallel execution
                        for i=NumWorkers:-1:1
                            BeamCell{i} = MTCopy(Obj);
                            FcnHandle{i} = @(x) MTOptimizeBinary(x, BeamCell{i}, ...
                                SymmetricTiling, TopoFormModular);
%                             FcnHandle{i} = @(x) Compliances(bi2de(x(:)')+1);
                        end
                    else
                        FcnHandle = @(x) MTOptimizeBinary(x, Obj, ...
                            SymmetricTiling, TopoFormModular);
%                         FcnHandle = @(x) Compliances(bi2de(x(:)')+1);
                    end

                    [COpt, ~, Misc] = MTGeneticAlgorithm(FcnHandle, Chromosome, GAOpt);
                    
                case 'enumeration'
                    EOpt.RangeMin = 0;
                    EOpt.RangeMax = 2^(numel(CWorst)-1)-1;
                    EOpt.UseParallel = true;
                    EOpt.DisplayFreq = 100;

                    if EOpt.UseParallel
                        NumWorkers = min([EOpt.RangeMax-EOpt.RangeMin, ...
                            java.lang.Runtime.getRuntime().availableProcessors]);       % number of logical cores
                    else
                        NumWorkers = 1;
                    end
                    
                    if NumWorkers>1                                                     % if parallel execution
                        for i=NumWorkers:-1:1
                            BeamCell{i} = MTCopy(Obj);
                            FcnHandle{i} = @(x) MTOptimizeBinary(x, BeamCell{i}, ...
                                SymmetricTiling, TopoFormModular);
                        end
                    else
                        FcnHandle = @(x) MTOptimizeBinary(x, Obj, ...
                            SymmetricTiling, TopoFormModular);
                    end
                    
                    [COpt, ~, Misc] = MTEnumerate(FcnHandle, CWorst, EOpt);
                    
                otherwise
                    error('MTSimpleBeam: Unknown AssemblyOptFormulation.');
            end

            % obtain optimal results
            MTOptimizeBinary(COpt, Obj, SymmetricTiling, TopoFormModular);
            BeamOpt = MTCopy(Obj); 
        end
            
    end

    methods(Static)
        function Out = MTSASelectNeighbor(Omega, Iter, IMax)
            % MTSASelectNeighbor function denotes a neighbor function for
            % Simulated Annealing
            %
            % ---INPUT-----------------------------------------------------
            %       Omega               binary connectivity matrix
            %       Iter                current iteration number
            %       IMax                maximum number of iterations
            %
            % ---OUTPUT----------------------------------------------------
            %       Out                 neighbor connectivity matrix
            
            Out = Omega;
            OmegaNaN = find(~isnan(Omega));                                 % find non-nan numbers
            NElem = length(OmegaNaN);                                       % count of non-nan numbers
            Y = max((1 - floor(NElem/2))/(0.9*IMax)*Iter + floor(NElem/2),1);
            NRevElem = ceil(Y);
            ToChange = randi([1 NRevElem], 1, 1);
            Pos = randperm(NElem, ToChange);
            Out(OmegaNaN(Pos)) = 1 - Out(OmegaNaN(Pos));
        end
    end
    
end

